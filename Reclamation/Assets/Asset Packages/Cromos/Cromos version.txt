CROMOS - outline and color animations changelog 

version 1.7.5
-- OUTLINE --
FIX: outline did not work properly on some mobile hardware

---------------------------------------------------------------------------------------------------

version 1.7.4
-- OUTLINE --
FIX: an error occurred when multiple outline effects where enabled and then disabled

---------------------------------------------------------------------------------------------------

version 1.7.3
-- OUTLINE --
FIX: performance optimization. In some cases garbage was produced

---------------------------------------------------------------------------------------------------

version 1.7.2
-- OUTLINE -- 
FIX: fixed issue Accurate Glow

-- COLOR ANIMATORS --
FIX: in some cases a compiling error about Gradient component occurred

---------------------------------------------------------------------------------------------------

version 1.7.1
-- OUTLINE --
ADD/FIX: now Outline Target support LOD groups: only the renderers of the current LOD are outlined
FIX: now outline works correctly with skinned meshe renderers with multiple sub-meshes
-- GLOBAL --
ADD: now Color Transition and Highlighter components can use shared materials instead of instanced materials
ADD: now it's possible to exclude particle system renderers from color transitions, highlighting and outline

---------------------------------------------------------------------------------------------------

version 1.7.0
-- OUTLINE --
PERFORMANCE: Outline system optimized to work with single render camera + command  buffer. It provides better performance.
ADD: 2 new outline modes added: Fast Solid and Fast Glow, optimized for mobile and low-end devices. Previous outline modes are now called Accurate Solid and Accurate Glow, optimized for PC and other mid/high-end devices.
ADD: It is now possible to switch outline between single game object and whole hierarchy
FIX: in some cases the rendered image of outlined object was vertically flipped

-- GLOBAL -- 
ADD: new After Transition Actions in Property Animator: DeactivateGameObject (formerly Deactivate), DestroyGameObject (formerly Destroy), DisableComponent, DestroyComponent)
CHANGE: PropertyAnimatorEvent moved to a new file

-- FULLSCREEN FADE -- 
FIX: now in FullscreenFade the fade sphere size is automatically computed correctly

---------------------------------------------------------------------------------------------------

previous version: 1.6.6
