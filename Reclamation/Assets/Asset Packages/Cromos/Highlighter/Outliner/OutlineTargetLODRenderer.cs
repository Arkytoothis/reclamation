﻿/********************************************************************
	created:	2018/03/20
	file base:	OutlineTargetLODRenderer
	file ext:	cs
	author:		Alessandro Maione
	version:	1.0.0
	
	purpose:	detects if a Renderer changes visibility  when outlining LOD objects, and communicates the change to OutlineTarget component
*********************************************************************/
using UnityEngine;

namespace Cromos
{
    /// <summary>
    /// detects if a Renderer changes visibility  when outlining LOD objects, and communicates the change to OutlineTarget component
    /// </summary>
    public class OutlineTargetLODRenderer : MonoBehaviour
    {
        public RendererVisibilityEvent OnVisibilityChanged = new RendererVisibilityEvent();

        void OnBecameVisible()
        {
            if ( OnVisibilityChanged != null )
                OnVisibilityChanged.Invoke( true );
        }

        void OnBecameInvisible()
        {
            if ( OnVisibilityChanged != null )
                OnVisibilityChanged.Invoke( false );
        }

    }
}
