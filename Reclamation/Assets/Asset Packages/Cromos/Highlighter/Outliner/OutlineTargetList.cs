﻿/********************************************************************
	created:	2018/01/30
	file base:	OutlineTargetList
	file ext:	cs
	author:		Alessandro Maione
	version:	1.0.1
	
	purpose:	an optimized list of outline  target objects
*********************************************************************/

using System.Collections.Generic;
using UnityEngine;

namespace Cromos
{
    /// <summary>
    /// an optimized list of outline  target objects
    /// </summary>
    public class OutlineTargetList : Dictionary<OutlineTargetListKey, HashSet<OutlineTarget>>
    {
        public OutlineTargetListEvent OnAdd = new OutlineTargetListEvent();
        public OutlineTargetListEvent OnRemove = new OutlineTargetListEvent();
        public OutlineTargetListEvent OnChange = new OutlineTargetListEvent();

        private int[] modeCounter = new int[OutlineModesConst.NumberOfOutlineModes];
        private int[] thicknessCounter = new int[OutlineTarget.MaxThickness - OutlineTarget.MinThickness + 1];



        /*
                public OutlineTargetList()
                {
                    ActiveModes = 0;
                    ActiveThickness = 0;
        //             foreach ( OutlineModes mode in Enum.GetValues( typeof( OutlineModes ) ) )
        //                 this[mode] = new Dictionary<int, HashSet<OutlineTarget>>();
                }*/

        /// <summary>
        /// adds outline target to a game object
        /// </summary>
        /// <param name="go">game object to which new outline target will be added</param>
        /// <param name="hidden">if true, the component will not be visible in editor</param>
        /// <returns>new outline target component</returns>
        public OutlineTarget AddOutlineTarget( GameObject go, bool hidden = false )
        {
            OutlineTarget target = null;

            if ( go == null )
                return target;

            target = go.GetComponent<OutlineTarget>();

            if ( !target )
            {
                target = go.AddComponent<OutlineTarget>();
                if ( hidden )
                    target.hideFlags = HideFlags.HideInInspector;
            }

            target.enabled = true;

            return target;
        }

        /// <summary>
        /// removes an outline target component from an object
        /// </summary>
        /// <param name="go">game object from which the outline target will be removed</param>
        /// <param name="destroy">if true, the component will be destroyed, else it will be just disabled</param>
        public void RemoveOutlineTarget( GameObject go, bool destroy = false )
        {
            if ( go != null )
            {
                OutlineTarget target = go.GetComponent<OutlineTarget>();

                if ( target )
                {
                    if ( destroy )
                        UnityEngine.Object.Destroy( target );
                    else
                        target.enabled = false;
                }
            }
        }

        public bool AddToList( OutlineTarget target )
        {
            bool result = false;
            OutlineTargetListKey key = new OutlineTargetListKey( target );
            if ( !ContainsKey( key ) )
                this[key] = new HashSet<OutlineTarget>();

            result = this[key].Add( target );

            if ( result )
            {
                modeCounter[(int)key.Mode]++;
                thicknessCounter[key.Thickness]++;
                OutlinePostEffect.Add().enabled = true;
            }

            if ( OnAdd != null )
                OnAdd.Invoke( target );

            return result;
        }

        public bool RemoveFromList( OutlineTarget target, bool oldThickness = false )
        {
            bool result = false;
            OutlineTargetListKey key = new OutlineTargetListKey( target, oldThickness );
            HashSet<OutlineTarget> targetSet = null;

            if ( ContainsKey( key ) )
                targetSet = this[key];


            if ( targetSet != null )
            {
                if ( targetSet.Contains( target ) )
                {
                    result = targetSet.Remove( target );

                    if ( targetSet.Count == 0 )
                        Remove( key );
                }
            }

            if ( result )
            {
                modeCounter[(int)key.Mode]--;
                thicknessCounter[key.Thickness]--;

                if ( OnRemove != null )
                    OnRemove.Invoke( target );

                if ( OutlineTarget.ActiveCounter == 0 )
                    if ( OutlinePostEffect.Instance )
                        OutlinePostEffect.Instance.enabled = false;
            }

            return result;
        }

        public OutlineTarget UpdateInList( OutlineTarget target )
        {
            if ( target.NeedsUpdate )
            {
                RemoveFromList( target, true );
                AddToList( target );

                if ( OnChange != null )
                    OnChange.Invoke( target );
            }

            return target;
        }

        public int CountByMode( OutlineModes mode )
        {
            return modeCounter[(int)mode];
        }

        public int CountByThickness( int thickness )
        {
            return thicknessCounter[Mathf.Clamp( thickness, OutlineTarget.MinThickness, OutlineTarget.MaxThickness )];
        }

        /*
                public static int EncodeOutlineTargetHash( OutlineTarget target )
                {
                    int thicknessRange = ( OutlineTarget.MaxThickness - OutlineTarget.MinThickness );
                    return ( (int)target.Mode * thicknessRange ) + target.OutlineThickness;
                }*/

        /*
                public static void DecodeOutlineTargetHash( int hash, out OutlineModes mode, out int thickness )
                {
                    //SBAGLIATA
                    int thicknessRange = ( OutlineTarget.MaxThickness - OutlineTarget.MinThickness );
                    mode = (OutlineModes)( hash / ( thicknessRange + 1 ) );
                    thickness = hash - ( (int)mode * thicknessRange );
                }*/

        #region DEBUG
        public void PrintContent()
        {
            foreach ( KeyValuePair<OutlineTargetListKey, HashSet<OutlineTarget>> pair in this )
            {
                Debug.Log( "MODE: " + pair.Key.Mode + " / THICKNESS: " + pair.Key.Thickness );
                foreach ( OutlineTarget ot in pair.Value )
                {
                    Debug.Log( "-- OUTLINE TARGET: " + ot.name );
                }
            }
        }
        #endregion

    }

}
