﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

/********************************************************************
created:	2016/09/30
file base:	Outliner/Post Outline
file ext:	shader
author:		Alessandro Maione
version:	1.6.8
purpose:	draws outline around non-black silhouettes rendered through DrawSilhouettes shader
*********************************************************************/
Shader"Outliner/Post Outline"
{
	Properties
	{
		_MainTex("Main render texture",2D) = "white"{}
		_SecondTex("Second render texture",2D) = "white"{}
		_Thickness("Outline thickness", Range(0.0, 30.0)) = 10.0
		//[Toggle] _Glow("Glow Mode: True=Glow / False=Solid", Int) = 0
		//_Color("Outline color", Color) = (1, 0, 0, 1)
		//[Toggle] _Flip("Flip", Int) = 0
	}

	CGINCLUDE
	#pragma target 3.0
	#include "UnityCG.cginc"
	
	#define MAX_THICKNESS  (20) /*VERY MAX CAN BE 32*/
	#define MAX_THICKNESS_AG_VERTICAL  (20*0.6)

	sampler2D _MainTex;
	sampler2D _SecondTex;
	float2 _MainTex_TexelSize;
	float2 _SecondTex_TexelSize;
	half _Thickness;
	//float _Glow;
	//fixed4 _Color;

	struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	v2f vert(appdata_img v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy;

		/*#ifdef UNITY_HALF_TEXEL_OFFSET
				v.texcoord.y += _MainTex_TexelSize.y;
		#endif*/
		#if UNITY_UV_STARTS_AT_TOP
		//#if SHADER_API_D3D11 || SHADER_API_D3D11_9X || SHADER_API_XBOXONE || SHADER_API_D3D9
			if (_MainTex_TexelSize.y < 0)
				o.uv.y = 1.0 - o.uv.y;
		//#endif
		#endif

		return o;
	}
		
		
	inline half4 getNeighborsSumHorizontal(float2 uv, half TX_x, half distance)
	{
		const half minDelta = 0.0039215686274509803921568627451;
		half normThickness = _Thickness / 255.0;

		//W
		half4 w = tex2D(_MainTex, uv + float2(-distance * TX_x, 0));
		w *= step(abs(w.a - normThickness), minDelta);
		w.a = (w.a > 0);

		//E
		half4 e = tex2D(_MainTex, uv + float2(distance * TX_x, 0));
		e *= step(abs(e.a - normThickness), minDelta);
		e.a = (e.a > 0);

		return w + e;
	}
	
	inline half4 getNeighborsSumVertical(float2 uv, half TX_y, half distance)
	{
		const half minDelta = 0.0039215686274509803921568627451;
		half normThickness = _Thickness / 255.0;

		//N
		half4 n = tex2D(_MainTex, uv + float2(0, distance * TX_y));
		n *= step(abs(n.a - normThickness), minDelta);
		n.a = (n.a > 0);

		//S
		half4 s = tex2D(_MainTex, uv + float2(0, -distance * TX_y));
		s *= step(abs(s.a - normThickness), minDelta);
		s.a = (s.a > 0);

		return n + s;
	}

	inline half4 getNeighborsSumHV(float2 uv, half TX_x, half TX_y, half distance)
	{
		const half minDelta = 0.0039215686274509803921568627451;
		half normThickness = _Thickness / 255.0;
		//N
		half4 n = tex2D(_SecondTex, uv + float2(0, distance * TX_y));
		n *= step(abs(n.a - normThickness), minDelta);
		n.a = (n.a > 0);

		//S
		half4 s = tex2D(_SecondTex, uv + float2(0, -distance * TX_y));
		s *= step(abs(s.a - normThickness), minDelta);
		s.a = (s.a > 0);

		//W
		half4 w = tex2D(_SecondTex, uv + float2(-distance * TX_x, 0));
		w *= step(abs(w.a - normThickness), minDelta);
		w.a = (w.a > 0);

		//E
		half4 e = tex2D(_SecondTex, uv + float2(distance * TX_x, 0));
		e *= step(abs(e.a - normThickness), minDelta);
		e.a = (e.a > 0);

		return n + s + w + e;
	}

	inline half4 getNeighborsSumOblique(float2 uv, half TX_x, half TX_y, half distance)
	{
		const half cos45 = 0.70710678118654752440084436210485;
		const half sin45 = 0.70710678118654752440084436210485;
		const half minDelta = 0.0039215686274509803921568627451;
		/*const half cos22_5 = 0.99997651217454865478849954406816;
		const half cos67_5 = 0.00685383828411102723918684180104;
		const half sin22_5 = 0.00685383828411102723918684180104;
		const half sin67_5 = 0.99997651217454865478849954406816;*/
		//			return (distance < 11) ?
		half normThickness = _Thickness / 255.0;

		//NW
		half4 nw = tex2D(_SecondTex, uv + float2(-distance * cos45 * TX_x, distance * sin45 * TX_y));
		nw *= step(abs(nw.a - normThickness), minDelta);
		nw.a = (nw.a > 0);

		//NE
		half4 ne = tex2D(_SecondTex, uv + float2(distance * cos45 * TX_x, distance * sin45 * TX_y));
		ne *= step(abs(ne.a - normThickness), minDelta);
		ne.a = (ne.a > 0);

		//SW
		half4 sw = tex2D(_SecondTex, uv + float2(-distance * cos45 * TX_x, -distance * sin45 * TX_y));
		sw *= step(abs(sw.a - normThickness), minDelta);
		sw.a = (sw.a > 0);

		//SE
		half4 se = tex2D(_SecondTex, uv + float2(distance * cos45 * TX_x, -distance * sin45 * TX_y));
		se *= step(abs(se.a - normThickness), minDelta);
		se.a = (se.a > 0);

		return nw + ne + sw + se;
		/*
						:

						//NW
						tex2D(_SecondTex, uv + float2(-distance * cos45 * TX_x, distance * sin45 * TX_y)) +
						//NE
						tex2D(_SecondTex, uv + float2(distance * cos45 * TX_x, distance * sin45 * TX_y)) +
						//SW
						tex2D(_SecondTex, uv + float2(-distance * cos45 * TX_x, -distance * sin45 * TX_y)) +
						//SE
						tex2D(_SecondTex, uv + float2(distance * cos45 * TX_x, -distance * sin45 * TX_y)) +
						//NNW
						tex2D(_SecondTex, uv + float2(-distance * cos67_5 * TX_x, distance * sin67_5 * TX_y)) +
						//NNE
						tex2D(_SecondTex, uv + float2(distance * cos67_5 * TX_x, distance * sin67_5 * TX_y)) +
						//ENE
						tex2D(_SecondTex, uv + float2(distance * cos22_5 * TX_x, distance * sin22_5 * TX_y)) +
						//ESE
						tex2D(_SecondTex, uv + float2(distance * cos22_5 * TX_x, -distance * sin22_5 * TX_y)) +
						//SSE
						tex2D(_SecondTex, uv + float2(distance * cos67_5 * TX_x, -distance * sin67_5 * TX_y)) +
						//SSW
						tex2D(_SecondTex, uv + float2(-distance * cos67_5 * TX_x, -distance * sin67_5 * TX_y)) +
						//WSW
						tex2D(_SecondTex, uv + float2(-distance * cos22_5 * TX_x, -distance * sin22_5 * TX_y)) +
						//WSW
						tex2D(_SecondTex, uv + float2(-distance * cos22_5 * TX_x, distance * sin22_5 * TX_y));*/
	}

	// PASS 0 - Horizontal Accurate Solid
	// _MainTex:  image of object seen by outline camera as white silhouettes
	// _SecondTexture: unused
	half4 fragHorizontalAccurateSolid(v2f i) : SV_Target
	{
		float2 uv = i.uv.xy;
		half4 result = half4(0,0,0,0);
		half4 silhouetteColor = tex2D(_MainTex, uv);
		
		if (any(silhouetteColor) || (_Thickness <= 0))
		{
			result = silhouetteColor;
			result.a = 1;
		}
		else
		{
			float2 deltax;
			for (int k = 0; k < MAX_THICKNESS; k++)
			{
				if ((half)k > _Thickness)
					break;
				
				silhouetteColor = getNeighborsSumHorizontal(uv, _MainTex_TexelSize.x, (half)k);
				//silhouetteColor /= silhouetteColor.a;
				if ( any(silhouetteColor))
				{
					result = silhouetteColor / silhouetteColor.a;
					result.a = 0;
					break;
				}
			}
		}
		
		return result;
	}
	
	// PASS 1 - Vertical And Blend Accurate Solid
	// _MainTex: source: original complete image
	// _SecondTexture: horizontal blurred image from first pass
	half4 fragVerticalAndBlendAccurateSolid(v2f i) : SV_Target
	{
		//#if UNITY_UV_STARTS_AT_TOP
		//			float2 uv = float2(i.uv.x, 1 - i.uv.y);
		//#else
		//			float2 uv = i.uv;
		//#endif
		float2 uv = i.uv.xy;
		half4 mainColor = tex2D(_MainTex, uv);
		half4 silhouetteColor = tex2D(_SecondTex, uv);

		if ((_Thickness <= 0) || (silhouetteColor.a >= 1))
			return mainColor;
		
		float TX_y = _SecondTex_TexelSize.y;
		half lerpVal = 0;
		float2 deltay;
		half4 up;
		half4 down;

		//for every iteration we need to do vertically
		for (int k = 1; k < MAX_THICKNESS; k++)
		{
			if ((half)k > _Thickness)
				break;

			deltay = float2(0, (half)k * TX_y);

			up = tex2D(_SecondTex, uv + deltay);
			down = tex2D(_SecondTex, uv - deltay);
			
			if ( any( up + down ))
			{
				silhouetteColor = any(up) ? up : down;
				lerpVal = 1;
				break;
			}
		}
		
		//lerpVal = 1;
		half4 result = half4(1,1,1,1);
		result.rgb = lerp(mainColor.rgb, silhouetteColor.rgb, lerpVal);
		return result;
	}

	// PASS 2 - Horizontal Accurate Glow
	// _MainTex:  image of object seen by outline camera as white silhouettes
	// _SecondTexture: unused
	half4 fragHorizontalAccurateGlow(v2f i) : SV_Target
	{
		half4 result = half4(0,0,0,0);
		float2 uv = i.uv.xy;
		half4 silhouetteColor = tex2D(_MainTex, uv);

		if (any(silhouetteColor) || (_Thickness <= 0))
		{
			result = silhouetteColor;
			result.a = 1;
			return result;
		}

		half inverseHalfThickness = 1.0 / _Thickness; // (MAX_THICKNESS);
		half2 deltax;
		half4 increment;

		//for every iteration we need to do horizontally
		for (int k = 1; k < MAX_THICKNESS; k++)
		{
			if ((half)k > _Thickness)
				break;
			
			silhouetteColor = getNeighborsSumHorizontal(uv, _MainTex_TexelSize.x, (half)k);
			silhouetteColor /= silhouetteColor.a;
			
			deltax = float2((float)k * _MainTex_TexelSize.x, 0);
			increment = inverseHalfThickness*silhouetteColor;
			result += any(tex2D(_MainTex, uv - deltax)) ? increment : half4(0,0,0,0);//half4(0,0.01,0,0);
			result += any(tex2D(_MainTex, uv + deltax)) ? increment : half4(0,0,0,0);//half4(0,0.01,0,0);
		}
		result.a = min(result.a, 0.99);

		return saturate(result);
	}

	// PASS 3 - Vertical And Blend Accurate Glow
	// _MainTex: source: original complete image
	// _SecondTexture: horizontal blurred image from first pass
	half4 fragVerticalAndBlendAccurateGlow(v2f i) : SV_Target
	{
		//#if UNITY_UV_STARTS_AT_TOP
		//			float2 uv = float2(i.uv.x, 1 - i.uv.y);
		//#else
		//			float2 uv = i.uv;
		//#endif

		float2 uv = i.uv.xy;
		half4 mainColor = tex2D(_MainTex, uv);
		half4 silhouetteColor = tex2D(_SecondTex, uv);

		if ((_Thickness <= 0) || (silhouetteColor.a >= 1))
			return mainColor;

		half4 result = mainColor;
		half TX_y = _SecondTex_TexelSize.y;
		//half inverseHalfThickness = 0.5 /  MAX_THICKNESS;
		float2 deltay;
		half lerpVal = 0;
		half4 up;
		half4 down;
		silhouetteColor=half4(0,0,0,0);
		half inverseT = 1/(_Thickness/**0.4*/);
		for (int k = 0; k < MAX_THICKNESS /*MAX_THICKNESS_AG_VERTICAL*/; k++)
		{
			if ((half)k > _Thickness/**0.4*/)
				break;

			deltay = float2(0, (float)k * TX_y);
			up = tex2D(_SecondTex, uv + deltay);
			down = tex2D(_SecondTex, uv - deltay);

			silhouetteColor.rgb = max ( silhouetteColor.rgb, max (up.rgb , down.rgb) );
			lerpVal += (inverseT/*inverseHalfThickness*/ * up.a);
			lerpVal += (inverseT/*inverseHalfThickness*/ * down.a);
		}
		silhouetteColor.a = 1;
		
		result.rgb = lerp(mainColor.rgb, silhouetteColor.rgb, saturate(lerpVal));
		//result.a = 1;

		return result;
	}

	// PASS 4 - Fast Solid
	// _MainTex: source image
	// _SecondTex: image of object seen by outline camera as white silhouettes
	half4 fragFastSolid(v2f i) : SV_Target
	{
		half4 result;
		float2 uv = i.uv.xy;
		half4 mainColor = tex2D(_MainTex, uv);
		float4 silhouetteColor = tex2D(_SecondTex, uv);

		if ( any(silhouetteColor) || (_Thickness <= 0) )
			result = mainColor;
		else
		{
			silhouetteColor = getNeighborsSumOblique(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness);
			silhouetteColor /= silhouetteColor.a;
			if (silhouetteColor.a > 0)
				result = saturate(silhouetteColor + half4(0, 0, 0, 1));
			else
			{
				silhouetteColor = getNeighborsSumHV(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness);
				silhouetteColor /= silhouetteColor.a;
				if (silhouetteColor.a > 0)
					result = saturate(silhouetteColor + half4(0, 0, 0, 1));
				else
					result = mainColor;
			}
		}

		return result;
		//				return any(getNeighborsSumHV(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness)) ? saturate(_Color + half4(0, 0, 0, 1)) :
		//				(any(getNeighborsSumOblique(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness)) ? saturate(_Color + half4(0, 0, 0, 1)) : mainColor);

						/*return (any(getNeighborsSumHV(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness) +
									getNeighborsSumOblique(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, _Thickness) )) ?
						saturate(_Color + half4(0,0,0,1)) : mainColor;*/
	}

	// PASS 5 - Fast Glow
	// _MainTex: source image
	// _SecondTex: image of object seen by outline camera as white silhouettes
	half4 fragFastGlow(v2f i) : SV_Target
	{
		float2 uv = i.uv.xy;
		half4 mainColor = tex2D(_MainTex, uv);
		half4 silhouettesColor = tex2D(_SecondTex, uv);
		half4 result = mainColor;

		if (any(silhouettesColor) || (_Thickness <= 0))
			return mainColor;
		else
		{
			half lerpVal = 0;
			for (int i = 1; i < MAX_THICKNESS; i++)
			{
				if ((half)i <= _Thickness)
				{
					lerpVal = 1.0 - ((half)i / _Thickness);
					silhouettesColor = getNeighborsSumHV(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, (half)i);
					silhouettesColor /= silhouettesColor.a;
					if (silhouettesColor.a > 0)
					{
						result = lerp(mainColor, silhouettesColor, lerpVal);
						break;
					}
					else
					{
						silhouettesColor = getNeighborsSumOblique(uv, _SecondTex_TexelSize.x, _SecondTex_TexelSize.y, (half)i);
						silhouettesColor /= silhouettesColor.a;
						if (silhouettesColor.a > 0)
						{
							result = lerp(mainColor, silhouettesColor, lerpVal);
							break;
						}
					}
				}
			}

			return result;
		}

		return result;
	}

	// PASS 6 - Flip
	half4 fragFlip(v2f i) : SV_Target
	{
		//#if UNITY_UV_STARTS_AT_TOP
		return tex2D(_MainTex, float2(i.uv.x, 1 - i.uv.y));
	//#else
	//			return tex2D(_MainTex, i.uv);
	//#endif
	}

	ENDCG


	SubShader
	{
		//NEVER ANABLE IT AGAIN! IT COUSES DIRTY RENDER TEXTURES
		//Blend SrcAlpha OneMinusSrcAlpha
		/*Blend Off
		ZTest Always
		Cull Off
		ZWrite Off
		Fog{ Mode Off }*/
		ZTest Always Cull Off ZWrite Off

		// 0 - Horizontal Accurate Solid pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragHorizontalAccurateSolid
			ENDCG
		}
		// end pass 0 - Horizontal Accurate Solid
		
		// 1 - vertical solid and blend pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragVerticalAndBlendAccurateSolid
			ENDCG
		}
		// end pass 1 - vertical solid and blend pass


		// 2 - Horizontal Accurate Glow pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragHorizontalAccurateGlow
			ENDCG
		}
		// end pass 2 - Horizontal Accurate Glow

		// 3 - Vertical And Blend Accurate Glow pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragVerticalAndBlendAccurateGlow
			ENDCG
		}
		// end pass 3 - Vertical And Blend Accurate Glow pass

		// 4 - Fast Solid pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragFastSolid
			ENDCG
		}
		// end pass 4 - Fast Solid

		// 5 - Fast Glow pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragFastGlow
			ENDCG
		}
		// end pass 5 - Fast Glow

		// 6 - Flip pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragFlip
			ENDCG
		}
		// end pass 6 - Flip

	}

}
//end shader
