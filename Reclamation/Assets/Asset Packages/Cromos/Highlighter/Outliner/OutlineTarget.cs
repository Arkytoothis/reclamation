﻿/********************************************************************
	created:	2016/11/20
	file base:	OutlineTarget
	file ext:	cs
	author:		Alessandro Maione
	version:	1.7.1
	purpose:	A game object with this component will been "seen" from the outline camera. It contains the color and the thickness of an element that will be outlined target
*********************************************************************/
using UnityEngine;

namespace Cromos
{
    /// <summary>
    /// A game object with this component will been "seen" from the outline camera.
    /// It contains the color and the thickness of an element that will be outlined
    /// </summary>
    public class OutlineTarget : MonoBehaviour
    {
        public const int MinThickness = 0;
        public const int MaxThickness = 20;

        /// <summary>
        /// list of all instantiated outline targets
        /// </summary>
        public static OutlineTargetList All
        {
            get
            {
                return all;
            }
        }
        private static OutlineTargetList all = new OutlineTargetList();

        [Header( "Options" )]
        public OutlineModes Mode = OutlineModes.FastSolid;
        /// <summary>
        /// thickness of outline around the object
        /// </summary>
        [Tooltip( "thickness of outline around the object" )]
        [Range( 0, MaxThickness )]
        public int Thickness = 2;
        /// <summary>
        /// color of outline around object
        /// </summary>
        [Tooltip( "color of outline around object" )]
        public Color32 OutlineColor = Color.red;
        public bool AffectChildren = true;
        [Tooltip( "if true, the particle systems will not outlined" )]
        /// <summary>
        /// if true, the particle systems will not outlined
        /// </summary>
        public bool ExcludeParticleSystems = true;

        public static int ActiveCounter
        {
            get;
            private set;
        }
        public bool NeedsUpdate
        {
            get
            {
                return ( ( Mode != OldMode ) ||
                         ( Thickness != OldThickness ) ||
                         ( !ColorHash.Color32Equals( OutlineColor, OldOutlineColor ) ) ||
                         ( AffectChildren != OldAffectChildren ) ||
                         ( ExcludeParticleSystems != OldExcludeParticleSystems ) ||
                         forceUpdate );
            }
        }
        private bool forceUpdate = true;

        /// <summary>
        /// this property should be used ONLY by OutlineTargetList
        /// </summary>
        //         public int MyHash
        //         {
        //             get;
        //             set;
        //         }

        public int OldThickness
        {
            get;
            private set;
        }
        public OutlineModes OldMode
        {
            get;
            private set;
        }
        public Color32 OldOutlineColor
        {
            get;
            private set;
        }
        public bool OldAffectChildren
        {
            get;
            private set;
        }
        public bool OldExcludeParticleSystems
        {
            get;
            private set;
        }

        private int myLayerOnStart = 0;
        private int outlineLayer = -1;
        private LODGroup lodGroup = null;
        private LOD[] lods = null;



        void Start()
        {
            myLayerOnStart = gameObject.layer;
            outlineLayer = LayerMask.NameToLayer( "Outline" );
            lodGroup = GetComponent<LODGroup>();
            if ( lodGroup )
            {
                lods = lodGroup.GetLODs();
                for ( int l = 0; l < lods.Length; l++ )
                {
                    LOD lod = lods[l];
                    for ( int r = 0; r < lod.renderers.Length; r++ )
                    {
                        if ( lod.renderers[r] )
                        {
                            OutlineTargetLODRenderer visEvent = lod.renderers[r].gameObject.AddComponent<OutlineTargetLODRenderer>();
                            visEvent.OnVisibilityChanged.AddListener( OnLODRendererVisibilityChanged );
                        }
                    }
                }
            }
        }

        void OnDestroy()
        {
            ResetLayer();
            All.RemoveFromList( this );
        }

        void OnEnable()
        {
            ActiveCounter++;
            OldThickness = -1;
            OldMode = OutlineModes.FastSolid;
            OldOutlineColor = Color.clear;
            OldAffectChildren = false;
            All.AddToList( this );
        }

        void OnDisable()
        {
            ActiveCounter--;
            ResetLayer();
            All.RemoveFromList( this );
        }

        private void LateUpdate()
        {
            //All.PrintContent();
            if ( NeedsUpdate )
                UpdateList();
        }

        private void UpdateList()
        {
            All.UpdateInList( this );
            //             All.RemoveFromList( this, true );
            //             All.AddToList( this );
            OldMode = Mode;
            OldThickness = Thickness;
            OldOutlineColor = OutlineColor;
            OldAffectChildren = AffectChildren;
            OldExcludeParticleSystems = ExcludeParticleSystems;
            forceUpdate = false;
        }

        /// <summary>
        /// sets the game object layer to 'Outline'. The layer of the object is temporary changed to apply outline post effect
        /// </summary>
        public void SetOutlineLayer()
        {
            SetOutlineLayer( AffectChildren );
        }

        /// <summary>
        /// sets the game object layer to 'Outline'. The layer of the object is temporary changed to apply outline post effect
        /// </summary>
        /// <param name="recursive">if true the layer is changed to game object in hierarchy too</param>
        public void SetOutlineLayer( bool recursive )
        {
            SetOutlineLayer( gameObject, recursive );
        }

        /// <summary>
        /// sets the game object layer to 'Outline'. The layer of the object is temporary changed to apply outline post effect
        /// </summary>
        /// <param name="go">target game object</param>
        /// <param name="recursive">if true the layer is changed to game object in hierarchy too</param>
        public void SetOutlineLayer( GameObject go, bool recursive )
        {
            if ( outlineLayer < 0 )
                Debug.LogError( @"in order to use outline effect add a new layer called 'Outline' in editor 'Tags and Layers'" );
            else
                go.layer = outlineLayer;

            if ( recursive )
                foreach ( Transform child in go.transform )
                    SetOutlineLayer( child.gameObject, recursive );
            //SetOutlineLayerLODGroup();
        }

        /// <summary>
        /// resets the game object layer to the original one
        /// </summary>
        public void ResetLayer()
        {
            ResetLayer( AffectChildren );
        }

        /// <summary>
        /// resets the game object layer to the original one
        /// </summary>
        /// <param name="recursive">if true the layer is changed to game object in hierarchy too</param>
        public void ResetLayer( bool recursive )
        {
            ResetLayer( gameObject, recursive );
        }

        /// <summary>
        /// resets the game object layer to the original one
        /// </summary>
        /// <param name="go">target game object</param>
        /// <param name="recursive">if true the layer is changed to game object in hierarchy too</param>
        public void ResetLayer( GameObject go, bool recursive )
        {
            go.layer = myLayerOnStart;

            if ( recursive )
                foreach ( Transform child in go.transform )
                    ResetLayer( child.gameObject, recursive );
        }

        private void SetOutlineLayerLODGroup()
        {
            if ( lodGroup )
            {
                if ( lods == null )
                {
                    lods = lodGroup.GetLODs();
                }
                for ( int l = 0; l < lods.Length; l++ )
                {
                    LOD lod = lods[l];
                    Renderer rend = null;
                    for ( int r = 0; r < lod.renderers.Length; r++ )
                    {
                        rend = lod.renderers[r];
                        if ( !rend.isVisible )
                            ResetLayer( rend.gameObject, false );
                    }
                }
            }
        }

        private void OnLODRendererVisibilityChanged( bool visibility )
        {
            forceUpdate = true;
        }

        #region DEBUG
        public override string ToString()
        {
            return name + " - " + base.ToString();
        }
        #endregion

    }

}


//HIGH: per ottenere la sovrapposizione di vari outline, si potrebbe usare il blend mode alpha|1-alpha
