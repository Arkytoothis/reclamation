﻿using Reclamation.Equipment;
using Reclamation.Gui.World;
using Reclamation.Misc;
using Reclamation.World;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileManager : Singleton<StockpileManager>
{
    [SerializeField] ResourcePanel resourcePanel;

    [SerializeField] List<ItemData> items;
    public List<ItemData> Items { get { return items; } }

    [SerializeField] List<ResourceData> resources;
    public List<ResourceData> Resources { get { return resources; } }

    public int MaxItems = 64;

    public void Initialize()
    {
        items = new List<ItemData>();

        for (int i = 0; i < MaxItems; i++)
        {
            items.Add(null);
        }

        GenerateRandomItems();

        resources = new List<ResourceData>();

        resources.Add(new ResourceData(Database.Resources["Coin"], 100, 1));
        resources.Add(new ResourceData(Database.Resources["Influence"], 0, 0));
        resources.Add(new ResourceData(Database.Resources["Supply"], 10, 0));
        resources.Add(new ResourceData(Database.Resources["Ration"], 20, 2));
        resources.Add(new ResourceData(Database.Resources["Material"], 100, 0));
        resources.Add(new ResourceData(Database.Resources["Tool"], 5, 0));
        resources.Add(new ResourceData(Database.Resources["Knowledge"], 0, 0));
        resources.Add(new ResourceData(Database.Resources["Essence"], 0, 0));
        resources.Add(new ResourceData(Database.Resources["Luxury"], 0, 0));
        resources.Add(new ResourceData(Database.Resources["Rarity"], 0, 0));

        resourcePanel.Initialize();
    }

    public void GenerateRandomItems()
    {
        int numItems = 30;

        for (int i = 0; i < numItems; i++)
        {
            AddItem(ItemGenerator.CreateRandomItem(ItemTypeAllowed.Any, 10, 10));
            //Debug.Log(items[i].Name + " generated");
        }
    }

    public void AddItem(ItemData item)
    {
        for (int i = 0; i < MaxItems; i++)
        {
            if (items[i] == null)
            {
                items[i] = new ItemData(item);
                break;
            }
        }
    }

    public void ModifyResource(int index, int amount)
    {
        resources[index].CurAmount += amount;
    }

    public void UpdateTime()
    {
        for (int i = 0; i < resources.Count; i++)
        {
            resources[i].CurAmount += resources[i].CurIncome;

            if (resources[i].CurAmount > resources[i].MaxAmount)
                resources[i].CurAmount = resources[i].MaxAmount;
        }

        resourcePanel.UpdateResources();
    }
}
