﻿using Reclamation.Equipment;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.World
{
    [System.Serializable]
    public class BuildingDefinition
    {
        public string Name;
        public string Key;
        public string Description;
        public string Icon;
        public bool Unlocked;

        public int DurabilityMax;
        public int HoursToBuild;
        public List<ResourceRequirement> ResourcesToBuild;
        public List<ResourceRequirement> ResourcesUsed;
        public List<ResourceRequirement> ResourcesGenerated;

        public BuildingDefinition()
        {
            Name = "";
            Key = "";
            Icon = "";
            Unlocked = false;

            DurabilityMax = 0;
            HoursToBuild = 0;
            Description = "";
        }

        public BuildingDefinition(string name, string key, string icon, int durability, int hours, bool unlocked, string description)
        {
            Name = name;
            Key = key;
            Icon = icon;
            DurabilityMax = durability;
            HoursToBuild = hours;
            Unlocked = unlocked;
            Description = description;
        }

        public string GetTooltipText()
        {
            string details = "Max Durability " + DurabilityMax;
            details += "\nHours to build " + HoursToBuild;

            details += "\nCost ";
            foreach (ResourceRequirement resource in ResourcesToBuild)
            {
                details += " " + resource.CurAmount + " " + resource.Key;
            }

            details += "\nMaintance ";
            foreach (ResourceRequirement resource in ResourcesUsed)
            {
                details += " " + resource.Key + " -" + resource.CurAmount;
            }

            details += "\nIncome ";
            foreach (ResourceRequirement resource in ResourcesGenerated)
            {
                details += " " + resource.Key + " +" + resource.CurAmount;
            }

            return details;
        }
    }
}