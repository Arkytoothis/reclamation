﻿using Reclamation.Equipment;
using Reclamation.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.World
{
    [System.Serializable]
    public class ResourceData
    {
        public string Key;
        public int CurAmount;
        public int MinAmount;
        public int MaxAmount;
        public int CurIncome;
        public int MinIncome;
        public int MaxIncome;

        public ResourceData()
        {
            Key = "";
            CurAmount = 0;
            MinAmount = 0;
            MaxAmount = 0;
            CurIncome = 0;
            MinIncome = 0;
            MaxIncome = 0;
        }

        public ResourceData(ResourceData data)
        {
            Key = data.Key;
            CurAmount = data.CurAmount;
            MinAmount = data.MinAmount;
            MaxAmount = data.MaxAmount;
            CurIncome = data.CurIncome;
            MinIncome = data.MinIncome;
            MaxIncome = data.MaxIncome;
        }

        public ResourceData(ResourceDefinition def)
        {
            Key = def.Key;
            CurAmount = 0;
            MinAmount = def.MinAmount;
            MaxAmount = def.MaxAmount;
            CurIncome = 0;
            MinIncome = def.MinIncome;
            MaxIncome = def.MaxIncome;
        }

        public ResourceData(ResourceDefinition def, int curAmount, int curIncome)
        {
            Key = def.Key;
            CurAmount = curAmount;
            MinAmount = def.MinAmount;
            MaxAmount = def.MaxAmount;
            CurIncome = curIncome;
            MinIncome = def.MinIncome;
            MaxIncome = def.MaxIncome;
        }
    }
}