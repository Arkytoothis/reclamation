﻿using System.Collections.Generic;
using UnityEngine;
using Reclamation.Gui.World;
using Reclamation.Misc;
using Reclamation.Characters;
using Reclamation.Party;
using Reclamation.Props;
using Pathfinding;
using FoW;
using Pathfinding.RVO;

namespace Reclamation.World
{
    public class PlayerManager : Singleton<PlayerManager>
    {
        [SerializeField] int maxParties = 6;
        [SerializeField] int maxPcs = 20;

        [SerializeField] Transform inactivePcsParent;
        [SerializeField] Transform partySpawn;

        [SerializeField] GameObject partyPrefab;

        //[SerializeField] int numPartiesUnlocked;
        [SerializeField] List<GameObject> parties;
        [SerializeField] List<GameObject> pcs;

        [SerializeField] PartyPanel partyPanel;

        public List<GameObject> Parties { get { return parties; } }
        public List<GameObject> Pcs { get { return pcs; } }

        public int MaxParties { get { return maxParties; } }
        public int MaxPcs {  get { return maxPcs; } }

        public void Initialize()
        {
            parties = new List<GameObject>(maxParties);
            pcs = new List<GameObject>();

            //numPartiesUnlocked = 1;

            CreateParty(partySpawn.position);

            PortraitRoom.instance.Initialize();

            CreatePc(0, 0, Gender.Male, "Imperial", "Soldier", Database.Races["Imperial"].maleDefaultHair, Database.Races["Imperial"].maleDefaultBeard);
            CreatePc(1, 0, Gender.Female, "Ogrin", "Soldier", Database.Races["Ogrin"].femaleDefaultHair, Database.Races["Ogrin"].femaleDefaultBeard);
            CreatePc(2, 0, Gender.Male, "High Elf", "Scout", Database.Races["High Elf"].maleDefaultHair, Database.Races["High Elf"].maleDefaultBeard);
            CreatePc(3, 0, Gender.Male, "Halfling", "Rogue", Database.Races["Halfling"].maleDefaultHair, Database.Races["Halfling"].maleDefaultBeard);
            CreatePc(4, 0, Gender.Female, "Mountain Dwarf", "Priest", Database.Races["Mountain Dwarf"].femaleDefaultHair, Database.Races["Mountain Dwarf"].femaleDefaultBeard);
            CreatePc(5, 0, Gender.Female, "High Elf", "Apprentice", Database.Races["High Elf"].femaleDefaultHair, Database.Races["High Elf"].femaleDefaultBeard);
           

            for (int i = 0; i < 6; i++)
            {
                string rndRace = Helper.RandomKey<string, Race>(Database.Races);
                string rndProfession = Helper.RandomKey<string, Profession>(Database.Professions);
                string hair = "";
                string beard = "";

                Gender rndGender = Gender.None;
                if (Random.Range(0, 2) == 0)
                {
                    rndGender = Gender.Male;
                    hair = Database.Races[rndRace].maleDefaultHair;
                    beard = Database.Races[rndRace].maleDefaultBeard;
                }
                else
                {
                    rndGender = Gender.Female;
                    hair = Database.Races[rndRace].femaleDefaultHair;
                    beard = Database.Races[rndRace].femaleDefaultBeard;
                }

                CreatePc(pcs.Count, -1, rndGender, rndRace, rndProfession, hair, beard);
                DeactivatePc(pcs.Count - 1);
            }

            for (int i = 0; i < 4; i++)
            {
                string rndRace = Helper.RandomKey<string, Race>(Database.Races);
                string hair = "";
                string beard = "";

                Gender rndGender = Gender.None;
                if (Random.Range(0, 2) == 0)
                {
                    rndGender = Gender.Male;
                    hair = Database.Races[rndRace].maleDefaultHair;
                    beard = Database.Races[rndRace].maleDefaultBeard;
                }
                else
                {
                    rndGender = Gender.Female;
                    hair = Database.Races[rndRace].femaleDefaultHair;
                    beard = Database.Races[rndRace].femaleDefaultBeard;
                }

                CreatePc(pcs.Count, -1, rndGender, rndRace, "Citizen", hair, beard);
                DeactivatePc(pcs.Count - 1);
            }

            //Camera.main.GetComponent<CameraController>().SetTarget(pcs[0].transform);
            PartyCursor.instance.PlaceMoveCursor(pcs[0].transform.position);

            partyPanel.Initialize();
        }

        void CreateParty(Vector3 spawnPosition)
        {
            float y = Terrain.activeTerrain.SampleHeight(new Vector3(spawnPosition.x, 0, spawnPosition.z));
            spawnPosition = new Vector3(spawnPosition.x, y + 2, spawnPosition.z);

            GameObject partyObject = Instantiate(partyPrefab);
            partyObject.transform.position = spawnPosition;

            parties.Add(partyObject);

            PartyData newParty = parties[0].GetComponent<PartyData>();
            newParty.SetPartyData("Blue Party", Color.blue, 0);
        }

        void CreatePc(int index, int party, Gender gender, string race, string profession, string hair, string beard)
        {
            GameObject pcObject = null;

            if (party == -1)
            {
                pcObject = ModelManager.instance.SpawnCharacter(inactivePcsParent, inactivePcsParent.position, gender, race, hair, beard);
                pcObject.GetComponent<AIDestinationSetter>().target = null;
            }
            else
            {
                pcObject = ModelManager.instance.SpawnCharacter(parties[party].transform, parties[party].transform.position, gender, race, hair, beard);
                pcObject.GetComponent<AIDestinationSetter>().target = null;
            }

            if (pcObject == null)
            {
                Debug.Log("pcObject == null");
            }

            PcData pcData = PcGenerator.Generate(pcObject, index, gender, race, profession);
            pcData.SetIsDead(false);

            GameObject model = ModelManager.instance.SpawnModel(PortraitRoom.instance.PcMounts[index].Pivot, gender, race, hair, beard, true);
            model.name = pcData.Name.ShortName + " portrait model";
            model.AddComponent<PcPortraitController>();

            ModelManager.instance.EquipCharacter(pcObject, pcData);
            ModelManager.instance.EquipCharacter(model, pcData);

            pcs.Add(pcObject);
        }

        public PartyData GetPartyData(int index)
        {
            if (parties[index].GetComponent<PartyData>() != null)
                return parties[index].GetComponent<PartyData>();
            else
                return null;
        }

        public PcData GetPcData(int index)
        {
            if (pcs[index] != null)
                return pcs[index].GetComponent<PcData>();
            else
                return null;
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                for (int i = 0; i < pcs.Count; i++)
                {
                    if (pcs[i] != null)
                    {
                        pcs[i].GetComponent<PcController>().CurrentDefense.Heal(1000);
                    }
                }
            }
            else if (Input.GetKeyUp(KeyCode.K))
            {
                for (int i = 0; i < pcs.Count; i++)
                {
                    if (pcs[i] != null)
                    {
                        pcs[i].GetComponent<PcController>().CurrentDefense.Damage(1000);
                    }
                }
            }

            //if (Input.GetKeyUp(KeyCode.F1))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(0);
            //}
            //else if (Input.GetKeyUp(KeyCode.F2))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(1);
            //}
            //else if (Input.GetKeyUp(KeyCode.F3))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(2);
            //}
            //else if (Input.GetKeyUp(KeyCode.F4))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(3);
            //}
            //else if (Input.GetKeyUp(KeyCode.F5))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(4);
            //}
            //else if (Input.GetKeyUp(KeyCode.F6))
            //{
            //    EncounterPartyManager.instance.SetCurrentPc(5);
            //}
        }

        public void DeactivatePc(int index)
        {
            pcs[index].GetComponent<PcAnimator>().enabled = false;
            pcs[index].GetComponent<FogOfWarUnit>().enabled = false;
            pcs[index].GetComponent<PcController>().enabled = false;
            pcs[index].GetComponent<AIDestinationSetter>().enabled = false;
            pcs[index].GetComponent<Seeker>().enabled = false;
            pcs[index].GetComponent<SimpleSmoothModifier>().enabled = false;
            pcs[index].GetComponent<RVOController>().enabled = false;
            pcs[index].GetComponent<RichAI>().enabled = false;
            pcs[index].GetComponent<Damagable>().enabled = false;
            pcs[index].GetComponent<MeleeAttack>().enabled = false;
        }
    }
}
