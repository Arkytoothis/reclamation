﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Reclamation.Party;
using Reclamation.World;
using TMPro;

namespace Reclamation.Gui.World
{
    public class PartyPanel : MonoBehaviour
    {
        [SerializeField] TMP_Text nameLabel;

        [SerializeField] List<PartyPcElement> pcElements = new List<PartyPcElement>(6);
        [SerializeField] GameObject pcElementPrefab;
        [SerializeField] Transform pcElementParent;

        public void Initialize()
        {
            if (PlayerManager.instance.GetPartyData(0) != null)
            {
                for (int i = 0; i < 6; i++)
                {
                    GameObject elementObject = Instantiate(pcElementPrefab, pcElementParent);

                    PartyPcElement elementScript = elementObject.GetComponent<PartyPcElement>();
                    elementScript.SetData(null);

                    pcElements.Add(elementScript);
                }
            }

            UpdateData();
        }

        public void UpdateData()
        {
            nameLabel.text = PlayerManager.instance.GetPartyData(0).Name;

            for (int i = 0; i < pcElements.Count; i++)
            {
                if (i < PlayerManager.instance.Pcs.Count)
                {
                    pcElements[i].GetComponent<PartyPcElement>().SetData(PlayerManager.instance.GetPcData(i));
                }
                else
                {
                    pcElements[i].GetComponent<PartyPcElement>().SetData(null);
                }
            }
        }
    }
}