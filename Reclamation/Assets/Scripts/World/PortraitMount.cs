﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Reclamation.Misc
{
    [System.Serializable]
    public class PortraitMount : MonoBehaviour
    {
        [SerializeField] Transform pivot;
        [SerializeField] Light spotLight;
        [SerializeField] Light pointLightLeft;
        [SerializeField] Light pointLightRight;

        [SerializeField] Camera closeCamera;
        [SerializeField] Camera farCamera;
        [SerializeField] RenderTexture rtClose;
        [SerializeField] RenderTexture rtFar;

        public Transform Pivot { get { return pivot; } }
        public Camera CloseCamera { get { return closeCamera; } }
        public RenderTexture RtClose { get { return rtClose; } }
        public Camera FarCamera { get { return farCamera; } }
        public RenderTexture RtFar { get { return rtFar; } }

        public void Setup()
        {
            rtClose = new RenderTexture(512, 512, 32);
            rtFar = new RenderTexture(512, 512, 32);

            CloseCamera.targetTexture = rtClose;
            FarCamera.targetTexture = rtFar;
        }
    }
}