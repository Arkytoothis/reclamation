﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Reclamation.Gui;
using FoW;
using Pathfinding;
using Reclamation.Characters;
using Reclamation.World;
using Cromos;

namespace Reclamation.Misc
{
    public class PortraitRoom : Singleton<PortraitRoom>
    {
        [SerializeField] GameObject mountPrefab;
        [SerializeField] List<PortraitMount> pcMounts;
        [SerializeField] Transform mountsParent;

        public List<PortraitMount> PcMounts { get { return pcMounts;  } }

        void Awake()
        {
            Reload();
        }

        public void Initialize()
        {
            pcMounts = new List<PortraitMount>();

            for (int i = 0; i < PlayerManager.instance.MaxPcs; i++)
            {
                AddPcMount(new Vector3(-(i * 2), 0, 0), i);
            }
        }

        public void AddPcMount(Vector3 position, int index)
        {
            GameObject mountObject = Instantiate(mountPrefab);
            mountObject.transform.SetParent(mountsParent, false);
            mountObject.transform.localPosition = position;

            pcMounts.Add(mountObject.GetComponent<PortraitMount>());

            PortraitMount mountScript = mountObject.GetComponent<PortraitMount>();
            mountScript.Setup();
        }

        public void AddModel(GameObject model, int index)
        {
            model.transform.SetParent(pcMounts[index].Pivot);
            model.transform.localPosition = Vector3.zero;
            model.name = model.name;
        }
    }
}