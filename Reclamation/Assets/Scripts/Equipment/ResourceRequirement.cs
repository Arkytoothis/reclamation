﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Equipment
{
    [System.Serializable]
    public class ResourceRequirement
    {
        public string Key;
        public int CurAmount;

        public ResourceRequirement()
        {
            Key = "";
            CurAmount = 0;
        }

        public ResourceRequirement(string resource, int amount)
        {
            Key = resource;
            CurAmount = amount;
        }

        public ResourceRequirement(ResourceRequirement req)
        {
            Key = req.Key;
            CurAmount = req.CurAmount;
        }
    }
}