﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Equipment
{
    [System.Serializable]
    public class ResourceDefinition
    {
        public string Name;
        public string Key;
        public string NamePlural;
        public string Icon;
        public string Description;
        public int Index;
        public int MinAmount;
        public int MaxAmount;
        public int MinIncome;
        public int MaxIncome;

        public ResourceDefinition()
        {
            Name = "";
            NamePlural = "";
            Key = "";
            Icon = "";
            Description = "";
            Index = -1;
            MinAmount = 0;
            MaxAmount = 0;
            MinIncome = 0;
            MaxIncome = 0;
        }

        public ResourceDefinition(string name, string key, string namePlural, string icon, string description, int index, int minAmount, int maxAmount, int minIncome, int maxIncome)
        {
            Name = name;
            NamePlural = namePlural;
            Key = key;
            Icon = icon;
            Description = description;
            Index = index;
            MinAmount = minAmount;
            MaxAmount = maxAmount;
            MinIncome = minIncome;
            MaxIncome = maxIncome;
        }
    }
}