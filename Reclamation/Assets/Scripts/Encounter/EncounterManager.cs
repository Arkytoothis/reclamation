﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Audio;
using Reclamation.Characters;
using Reclamation.Equipment;
using Reclamation.Misc;
using Reclamation.Gui;
using Reclamation.Gui.Encounter;
using Guirao.UltimateTextDamage;

namespace Reclamation.Encounter
{
    public class EncounterManager : Singleton<EncounterManager>
    {        
        public UltimateTextDamageManager textManagerWorld;

        void Awake()
        {
            Invoke(nameof(Initialize), 0.1f);
        }

        public void Initialize()
        {
            Database.Initialize();
            ItemGenerator.Initialize();
            PcGenerator.Initialize();
            NpcGenerator.Initialize();

            SpriteManager.instance.Initialize();
            AudioManager.instance.Initialize();
            ModelManager.instance.Initialize();
            ParticleManager.instance.Initialize();
            PlayerManager.instance.Initialize();

            EncounterGuiManager.instance.Initialize();
            PlayerManager.instance.SelectPc(0);
            AudioManager.instance.WorldPlaylist.StartPlaying(0);
            AudioManager.instance.PlayAmbient("castle abandoned");
        }
    }
}