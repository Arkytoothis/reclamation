﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Encounter;
using Reclamation.Misc;

namespace Reclamation.Gui.Encounter
{
    public class EncounterGuiManager : Singleton<EncounterGuiManager>
    {
        public PartyPanel partyPanel;
        public CharacterScreen characterViewer;
        public CharacterPanel characterPanel;

        public void Initialize()
        {
            TooltipManager.instance.Initialize();

            partyPanel.Initialize();
            characterViewer.Initialize();
            characterPanel.Initialize();
        }

        public void SelectPc(int index)
        {
            partyPanel.SelectPc(index);
            characterViewer.SetData(PlayerManager.instance.GetPcData(index));
            characterPanel.SetData(PlayerManager.instance.GetPcData(index));
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.C))
            {
                characterViewer.Toggle();
            }
        }
    }
}