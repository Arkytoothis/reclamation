﻿using Reclamation.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterCursor : Singleton<EncounterCursor>
{
    [SerializeField] bool isEnabled = true;

    [SerializeField] Camera cam;
    [SerializeField] CameraRaycaster raycaster;

    public bool IsEnabled
    {
        get { return isEnabled; }
        set { isEnabled = value; }
    }

    void Awake()
    {
        cam = Camera.main;
        raycaster = cam.GetComponent<CameraRaycaster>();
    }

    void Start()
    {
        raycaster.onMouseOverWalkable += OnMouseOverWalkable;
    }

    public bool OnMouseOverWalkable(RaycastHit hit)
    {
        transform.position = hit.point;

        return true;
    }
}
