﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Misc;
using Reclamation.Party;
using Pathfinding;
using Reclamation.Characters;
using Reclamation.Gui.Encounter;

namespace Reclamation.Encounter
{
    public class PlayerManager : Singleton<PlayerManager>
    {
        [SerializeField] GameObject emptyPc;
        [SerializeField] List<GameObject> pcs;

        public List<GameObject> Pcs { get { return pcs; } }

        public void Initialize()
        {
            pcs = new List<GameObject>();

            CreatePc(0, Gender.Male, "Imperial", "Soldier", "hair 13", "beard 01");
            CreatePc(1, Gender.Female, "Ogrin", "Soldier", "hair 14", "");
            CreatePc(2, Gender.Male, "High Elf", "Scout", "hair 15", "");
            CreatePc(3, Gender.Male, "Halfling", "Rogue", "hair 13", "");
            CreatePc(4, Gender.Female, "Mountain Dwarf", "Priest", "hair 14", "");
            CreatePc(5, Gender.Female, "High Elf", "Apprentice", "hair 15", "");
        }

        public void CreatePc(int index, Gender gender, string race, string profession, string hair, string beard)
        {
            GameObject pcObject = ModelManager.instance.SpawnCharacter(transform, transform.position, gender, race, hair, beard);
            pcObject.GetComponent<AIDestinationSetter>().target = null;

            PcData pcData = PcGenerator.Generate(pcObject, index, gender, race, profession);
            pcData.SetIsDead(false);

            GameObject model = ModelManager.instance.SpawnModel(PortraitRoom.instance.PcMounts[index].Pivot, gender, race, hair, beard, true);
            model.name = pcData.Name.ShortName + " portrait model";
            model.AddComponent<PcPortraitController>();

            ModelManager.instance.EquipCharacter(pcObject, pcData);
            ModelManager.instance.EquipCharacter(model, pcData);

            pcs.Add(pcObject);
        }

        public PcData GetPcData(int index)
        {
            if (pcs[index] != null)
                return pcs[index].GetComponent<PcData>();
            else
                return null;
        }

        public void SelectPc(int index)
        {
            Camera.main.GetComponent<CameraController>().SetTarget(pcs[index].transform);
            EncounterGuiManager.instance.SelectPc(index);
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.F1))
            {
                SelectPc(0);
            }
            else if (Input.GetKeyUp(KeyCode.F2))
            {
                SelectPc(1);
            }
            else if (Input.GetKeyUp(KeyCode.F3))
            {
                SelectPc(2);
            }
            else if (Input.GetKeyUp(KeyCode.F4))
            {
                SelectPc(3);
            }
            else if (Input.GetKeyUp(KeyCode.F5))
            {
                SelectPc(4);
            }
            else if (Input.GetKeyUp(KeyCode.F6))
            {
                SelectPc(5);
            }
        }
    }
}