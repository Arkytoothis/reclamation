﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Sandbox
{
    public enum SandboxMode { Character, NPC, Item }

    public class SandboxGuiManager : MonoBehaviour
    {
        [SerializeField] CharacterPanel characterPanel;
        [SerializeField] ItemPanel itemPanel; 

        public void Initialize()
        {
            characterPanel.Initialize();
            itemPanel.Initialize();

            characterPanel.Hide();
            itemPanel.Hide();
        }

        public void SetMode(SandboxMode mode)
        {
            switch (mode)
            {
                case SandboxMode.Character:
                    break;
                case SandboxMode.NPC:
                    break;
                case SandboxMode.Item:
                    break;
                default:
                    break;
            }
        }

        public void CharacterMode()
        {
            SandboxManager.instance.CharacterMode();
            characterPanel.Show();
            itemPanel.Hide();
        }

        public void NPCMode()
        {
            SandboxManager.instance.NPCMode();
            characterPanel.Hide();
            itemPanel.Hide();
        }

        public void ItemMode()
        {
            SandboxManager.instance.ItemMode();
            characterPanel.Hide();
            itemPanel.Show();
        }
    }
}