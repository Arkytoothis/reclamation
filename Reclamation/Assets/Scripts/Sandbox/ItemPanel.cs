﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Reclamation.Misc;
using TMPro;
using Reclamation.Characters;
using Reclamation.Equipment;
using System.IO;

namespace Reclamation.Sandbox
{
    public class ItemPanel : MonoBehaviour
    {
        [SerializeField] string currentItem;
        [SerializeField] TMP_Dropdown itemDropdown;
        [SerializeField] Transform itemMount;
        [SerializeField] float itemRotationSpeed = 10f;

        [SerializeField] bool portraitMode = true;
        [SerializeField] GameObject portraitBackground;
        [SerializeField] RawImage portraitImage;
        [SerializeField] Camera portraitCam;

        public bool RotateItemModel = true;

        public void Initialize()
        {
            itemDropdown.ClearOptions();

            foreach (KeyValuePair<string, GameObject> kvp in ModelManager.instance.ItemPrefabs)
            {
                itemDropdown.options.Add(new TMP_Dropdown.OptionData(kvp.Key));
            }

            itemDropdown.value = 0;
            itemDropdown.RefreshShownValue();

        }

        void Update()
        {
            if (RotateItemModel == true)
            {
                itemMount.Rotate(Vector3.up, itemRotationSpeed * 100f * Time.deltaTime);
            }
        }

        public void SetModel()
        {
            string modelKey = itemDropdown.options[itemDropdown.value].text;

            if (itemMount.childCount > 0)
            {
                itemMount.GetChild(0).transform.position = SandboxManager.instance.ItemModels[modelKey].transform.position;
                itemMount.GetChild(0).SetParent(SandboxManager.instance.ItemModels[modelKey].transform.parent);
            }

            SandboxManager.instance.ItemModels[modelKey].transform.position = itemMount.position;
            SandboxManager.instance.ItemModels[modelKey].transform.SetParent(itemMount);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void TogglePortraitMode()
        {
            portraitMode = !portraitMode;

            if (portraitMode == true)
            {
                portraitBackground.SetActive(true);
                RotateItemModel = false;
                itemMount.rotation = Quaternion.identity;
                portraitImage.texture = portraitCam.targetTexture;
            }
            else
            {
                portraitBackground.SetActive(false);
                RotateItemModel = true;
                itemMount.rotation = Quaternion.identity;
            }
        }

        public void RotateLeft()
        {
            itemMount.Rotate(Vector3.up, 90);
        }

        public void RotateRight()
        {
            itemMount.Rotate(Vector3.up, -90);
        }

        public void SavePortrait()
        {
            string path = "Assets\\Resources\\Textures\\Raw Item Icons\\";
            portraitImage.texture = portraitCam.targetTexture;

            var oldRT = RenderTexture.active;

            var tex = new Texture2D(portraitCam.targetTexture.width, portraitCam.targetTexture.height);
            RenderTexture.active = portraitCam.targetTexture;
            tex.ReadPixels(new Rect(0, 0, portraitCam.targetTexture.width, portraitCam.targetTexture.height), 0, 0);
            tex.Apply();

            File.WriteAllBytes(path + itemDropdown.options[itemDropdown.value].text + ".png", tex.EncodeToPNG());
            RenderTexture.active = oldRT;

            Debug.Log(path + itemDropdown.options[itemDropdown.value].text + ".png saved");
        }
    }
}