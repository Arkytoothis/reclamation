﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Reclamation.Misc;
using Reclamation.Equipment;
using Reclamation.Characters;

namespace Reclamation.Sandbox
{
    public class SandboxManager : Singleton<SandboxManager>
    {
        [SerializeField] SandboxGuiManager guiManager;


        [SerializeField] Transform characterModelsParent;
        [SerializeField] Transform hairModelsParent;
        [SerializeField] Transform beardModelsParent;
        [SerializeField] Transform itemModelsParent;

        [SerializeField] Transform defaultCameraMount;
        [SerializeField] Transform characterCameraMount;
        [SerializeField] Transform itemCameraMount;

        Dictionary<string, GameObject> characterModels;
        Dictionary<string, GameObject> hairModels;
        Dictionary<string, GameObject> beardModels;
        Dictionary<string, GameObject> itemModels;

        public Dictionary<string, GameObject> CharacterModels
        {
            get
            {
                return characterModels;
            }
        }

        public Dictionary<string, GameObject> ItemModels
        {
            get
            {
                return itemModels;
            }
        }

        void Awake()
        {
            Database.Initialize();
            ItemGenerator.Initialize();
            PcGenerator.Initialize();
            ModelManager.instance.Initialize();
            SpriteManager.instance.Initialize();

            characterModels = new Dictionary<string, GameObject>();
            hairModels = new Dictionary<string, GameObject>();
            beardModels = new Dictionary<string, GameObject>();
            itemModels = new Dictionary<string, GameObject>();

            LoadModels();
        }

        void Start()
        {
            Invoke(nameof(Initialize), 0.1f);
        }

        void Initialize()
        {
            DefaultMode();
            guiManager.Initialize();
        }

        public void DefaultMode()
        {
            Camera.main.transform.position = defaultCameraMount.position;
            Camera.main.transform.rotation = defaultCameraMount.rotation;
        }

        public void CharacterMode()
        {
            Camera.main.transform.position = characterCameraMount.position;
            Camera.main.transform.rotation = characterCameraMount.rotation;
        }

        public void ItemMode()
        {
            Camera.main.transform.position = itemCameraMount.position;
            Camera.main.transform.rotation = itemCameraMount.rotation;
        }

        public void NPCMode()
        {
        }

        public void LoadModels()
        {
            foreach (KeyValuePair<string, GameObject> kvp in ModelManager.instance.CharacterPrefabs)
            {
                GameObject go = Instantiate(kvp.Value, characterModelsParent);
                characterModels.Add(kvp.Key, go);
            }

            foreach (KeyValuePair<string, GameObject> kvp in ModelManager.instance.HairPrefabs)
            {
                GameObject go = Instantiate(kvp.Value, hairModelsParent);
                hairModels.Add(kvp.Key, go);
            }

            foreach (KeyValuePair<string, GameObject> kvp in ModelManager.instance.BeardPrefabs)
            {
                GameObject go = Instantiate(kvp.Value, beardModelsParent);
                beardModels.Add(kvp.Key, go);
            }

            foreach (KeyValuePair<string, GameObject> kvp in ModelManager.instance.ItemPrefabs)
            {
                GameObject go = Instantiate(kvp.Value, itemModelsParent);
                itemModels.Add(kvp.Key, go);
            }
        }
    }
}