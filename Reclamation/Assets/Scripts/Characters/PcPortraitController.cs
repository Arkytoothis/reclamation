﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Characters
{
    public class PcPortraitController : MonoBehaviour
    {
        [SerializeField] Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            if (animator != null)
            {
                StartCoroutine("WaitSeconds");
            }
        }

        IEnumerator WaitSeconds()
        {
            while (true)
            {
                float randomWait = Random.Range(10, 30);

                switch (Random.Range(1, 5))
                {
                    case 1:
                        animator.SetBool("attack", true);
                        break;
                    case 2:
                        animator.SetBool("levelUp", true);
                        break;
                    case 3:
                        animator.SetBool("interact", true);
                        break;
                    case 4:
                        animator.SetBool("die", true);
                        break;
                    default:
                        break;
                }

                yield return new WaitForSeconds(randomWait);
            }
        }
    }
}