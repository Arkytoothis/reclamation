﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Reclamation.Equipment;
using Reclamation.Misc;
using UnityEngine.Animations;

namespace Reclamation.Characters
{
    public class CharacterRenderer : MonoBehaviour
    {
        [SerializeField] Transform pivot;
        public Transform Pivot { get { return pivot; } }

        [SerializeField] List<Transform> mounts;
        public List<Transform> Mounts { get { return mounts; } }

        [SerializeField] List<SkinnedMeshRenderer> meshes;
        [SerializeField] PcData pcData;


        public void LoadEquipment(PcData pcData)
        {
            if (pcData == null) return;
            this.pcData = pcData;

            LoadItem((int)EquipmentSlot.Right_Hand, (int)CharacterRenderSlot.Right_Hand);
            LoadItem((int)EquipmentSlot.Left_Hand, (int)CharacterRenderSlot.Left_Hand);

            if (pcData.Hair != "")
                SetHair(ModelManager.instance.GetHairModel(pcData.Hair));
            if (pcData.Beard != "")
                SetBeard(ModelManager.instance.GetBeardModel(pcData.Beard));
        }

        public void LoadWeaponSet(int index)
        {

        }

        public void LoadItem(int equipmentSlot, int renderSlot)
        {
            ItemData item = this.pcData.Inventory.EquippedItems[equipmentSlot];

            if (item != null)
            {
                GameObject go = Instantiate(ModelManager.instance.GetItemPrefab(item.Key));

                if (go == null)
                {
                    Debug.Log("Loading " + item.Name + " to Right Hand failed");
                }
                else
                {
                    ItemDefinition def = Database.GetItem(item.Key, false);
                    go.transform.SetParent(mounts[renderSlot], true);
                    go.transform.localPosition = def.offset;
                    go.transform.localEulerAngles = def.rotation;
                    go.transform.localScale = Vector3.one;

                }
            }
        }

        public void SetHair(GameObject hair)
        {
            if (mounts[(int)CharacterRenderSlot.Hair].childCount > 0)
            {
                Destroy(mounts[(int)CharacterRenderSlot.Hair].GetChild(0).gameObject);
            }

            GameObject go = Instantiate(hair, Vector3.zero, Quaternion.identity);

            ModelAdjustment adjustment = go.GetComponent<ModelAdjustment>();
            if (adjustment != null)
            {
                go.transform.localPosition = adjustment.position;
                go.transform.localEulerAngles = adjustment.rotation;
                go.transform.localScale = adjustment.scale;
            }

            go.transform.SetParent(mounts[(int)CharacterRenderSlot.Hair], false);

            go.name = hair.name;
        }

        public void SetBeard(GameObject beard)
        {
            if (mounts[(int)CharacterRenderSlot.Face].childCount > 0)
            {
                Destroy(mounts[(int)CharacterRenderSlot.Face].GetChild(0).gameObject);
            }

            GameObject go = Instantiate(beard);

            ModelAdjustment adjustment = go.GetComponent<ModelAdjustment>();
            if (adjustment != null)
            {
                go.transform.localPosition = adjustment.position;
                go.transform.localEulerAngles = adjustment.rotation;
                go.transform.localScale = adjustment.scale;
            }

            go.transform.SetParent(mounts[(int)CharacterRenderSlot.Face], false);

            go.name = beard.name;
        }
    }
}