﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Reclamation.Gui
{
    [CreateAssetMenu(menuName = "Reclamation/Button")]
    public class ButtonData : ScriptableObject
    {
        [Header("Image Data")]
        public Sprite background;
        public Sprite icon;
        public ColorBlock buttonColors;
        public string text;

        [Header("Sound Data")]
        public string clickSound;
    }
}
