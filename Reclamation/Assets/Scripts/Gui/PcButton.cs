﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Misc;
using Reclamation.Characters;
using Reclamation.Encounter;
using Pathfinding;
using Pathfinding.RVO;
using UnityEngine.EventSystems;
using Reclamation.Audio;

namespace Reclamation.Gui
{
    public class PcButton : GuiElement
    {
        [SerializeField] PcButtonData skinData;
        [SerializeField] PcButtonData selectedSkinData;
        [SerializeField] Image background;
        [SerializeField] UnityEngine.UI.Button button;

        [SerializeField] TMP_Text nameLabel;
        [SerializeField] RawImage portrait;

        [SerializeField] GameObject attributeBarPrefab;
        [SerializeField] List<GameObject> attributeBars;
        [SerializeField] Transform attributeBarsParent;

        [SerializeField] GameObject expBar;

        [SerializeField] int index;
        //private PcData pc;

        protected override void OnSkinGui()
        {
            base.OnSkinGui();

            background.sprite = skinData.background;
            background.type = Image.Type.Sliced;

            button.transition = Selectable.Transition.ColorTint;
            button.colors = skinData.buttonColors;

            expBar.name = "Exp Bar";
        }

        public void OnPointerClick(BaseEventData bed)
        {
            //PointerEventData ped = (PointerEventData)bed;

            AudioManager.instance.PlaySound(skinData.clickSound, false);
            PlayerManager.instance.SelectPc(index);
        }

        public void Select()
        {
            button.colors = selectedSkinData.buttonColors;
        }

        public void Deselect()
        {
            button.colors = skinData.buttonColors;
        }

        public void SetData(int index, PcData pc)
        {
            this.index = index;

            if (pc != null)
            {
                //this.pc = pc;
                GameObject go = null;
                AttributeBar bar = null;
                nameLabel.text = pc.Name.FirstName;

                for (int i = 0; i < (int)Vital.Number; i++)
                {
                    go = Instantiate(attributeBarPrefab, attributeBarsParent);
                    bar = go.GetComponent<AttributeBar>();
                    bar.SetData(pc, i);

                    attributeBars.Add(go);
                }

                go = Instantiate(attributeBarPrefab, attributeBarsParent);
                bar = go.GetComponent<AttributeBar>();
                bar.SetExpData(pc);
                expBar = go;

                portrait.texture = PortraitRoom.instance.PcMounts[pc.EncounterIndex].CloseCamera.targetTexture;
            }
            else
            {
                nameLabel.text = "";
                portrait.texture = null;
            }
        }
    }
}