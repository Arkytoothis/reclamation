﻿using Reclamation.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class ArmoryStockpilePanel : Panel
    {
        [SerializeField] GameObject inventoryButtonPrefab;
        [SerializeField] Transform inventoryButtonsParent;
        [SerializeField] List<GameObject> inventoryButtons;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);

            inventoryButtons = new List<GameObject>();

            for (int i = 0; i < StockpileManager.instance.MaxItems; i++)
            {
                GameObject buttonObject = Instantiate(inventoryButtonPrefab, inventoryButtonsParent);

                InventoryButton buttonScript = buttonObject.GetComponent<InventoryButton>();
                buttonScript.Clear();
                inventoryButtons.Add(buttonObject);
            }

            LoadStockpile();
        }

        public void LoadStockpile()
        {
            for (int i = 0; i < StockpileManager.instance.MaxItems; i++)
            {
                InventoryButton buttonScript = inventoryButtons[i].GetComponent<InventoryButton>();
                buttonScript.SetItemData(StockpileManager.instance.Items[i]);
            }
        }
        
        public void SetPcData(PcData pcData)
        {
            if (pcData != null)
            {
            }
        }
    }
}