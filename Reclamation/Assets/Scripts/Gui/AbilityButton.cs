﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Audio;
using Reclamation.Gui;
using Reclamation.Equipment;
using Reclamation.Misc;
using UnityEngine.EventSystems;
using Reclamation.Abilities;

public class AbilityButton : GuiElement
{
    [SerializeField] Ability ability;
    [SerializeField] AbilityButtonData skinData;
    [SerializeField] Image abilityIcon;
    [SerializeField] UnityEngine.UI.Button button;

    [SerializeField] int index = -1;

    public int Index { get { return index; } }

    protected override void OnSkinGui()
    {
        base.OnSkinGui();

        button.transition = Selectable.Transition.ColorTint;
        button.colors = skinData.buttonColors;
    }

    public void OnPointerClick()
    {
        AudioManager.instance.PlaySound(skinData.clickSound, false);
    }

    public void Setup(int index, bool unlocked)
    {
        Clear();
        this.index = index;
    }

    public void SetAbilityData(Ability ability)
    {
        if (ability != null)
        {
            this.ability = new Ability(ability);
            abilityIcon.enabled = true;
            abilityIcon.sprite = SpriteManager.instance.GetAbilityIcon(ability.SpriteKey);
        }
        else
        {
            Clear();
        }
    }

    public void Clear()
    {
        ability = null;

        abilityIcon.enabled = false;
        index = -1;
    }

    public void Enable()
    {
        button.interactable = true;
    }

    public void Disable()
    {
        button.interactable = false;
    }

    public void OnPointerUp(BaseEventData bed)
    {
        //PointerEventData ped = (PointerEventData)bed;

        AudioManager.instance.PlaySound(skinData.clickSound, false);
    }

    public void OnPointerEnter(BaseEventData bed)
    {
        TooltipManager.instance.DisplayAbility(ability);
    }

    public void OnPointerExit(BaseEventData bed)
    {
        TooltipManager.instance.Hide();
    }
}
