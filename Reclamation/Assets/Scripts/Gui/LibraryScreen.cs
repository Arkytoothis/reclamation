﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui
{
    public class LibraryScreen : GameScreen
    {
        public override void Initialize()
        {
            //Debug.Log("LibraryScreen Initialized");
            base.Initialize();
        }

        public override void Open()
        {
            base.Open();
        }

        public override void Close()
        {
            base.Close();
        }
    }
}