﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Reclamation.Gui
{
    [CreateAssetMenu(menuName = "Reclamation/Inventory Button")]
    public class InventoryButtonData : ScriptableObject
    {
        [Header("Image Data")]
        public ColorBlock buttonColors;

        [Header("Sound Data")]
        public string clickSound;
    }
}