﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Characters;
using Reclamation.Encounter;
using TMPro;

namespace Reclamation.Gui.Encounter
{
    public class PartyPanel : MonoBehaviour
    {
        public GameObject pcElementPrefab;
        public List<GameObject> pcButtons;
        public Transform pcElementsParent;
        public TMP_Text label;

        public void Initialize()
        {
            for (int i = 0; i < PlayerManager.instance.Pcs.Count; i++)
            {
                GameObject go = Instantiate(pcElementPrefab, pcElementsParent);
                PcButton button = go.GetComponent<PcButton>();
                button.SetData(i, PlayerManager.instance.GetPcData(i));

                pcButtons.Add(go);
            }
        }

        public void SelectPc(int index)
        {
            for (int i = 0; i < pcButtons.Count; i++)
            {
                PcButton button = pcButtons[i].GetComponent<PcButton>();

                if (i == index)
                {
                    button.Select();
                }
                else
                {
                    button.Deselect();
                }
            }
        }

        public void UpdateLabel(int layer)
        {
            label.text = "Layer " + layer;
        }
	}
}