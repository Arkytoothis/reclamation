﻿using Reclamation.Characters;
using Reclamation.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class BarracksEquipmentPanel : Panel
    {
        [SerializeField] GameObject equippedItemPrefab;
        [SerializeField] Transform equippedItemsContainer;
        [SerializeField] List<GameObject> equippedItems;

        [SerializeField] GameObject accessoryPrefab;
        [SerializeField] Transform accessoriesContainer;
        [SerializeField] List<GameObject> accessories;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);
            SetupEquippedItemButtons();
            SetupEquippedWeaponSetButtons();
            SetupEquippedAccessoryButtons();
        }

        public void SetupEquippedItemButtons()
        {
            equippedItems = new List<GameObject>();

            for (int i = 0; i < (int)EquipmentSlot.Number; i++)
            {
                GameObject go = Instantiate(equippedItemPrefab, equippedItemsContainer);

                EquippedItemButton script = go.GetComponent<EquippedItemButton>();
                script.Setup(i, ((EquipmentSlotAbb)i).ToString());

                equippedItems.Add(go);
            }

            accessories = new List<GameObject>();

            for (int i = 0; i < CharacterInventory.MaximumAccessories; i++)
            {
                GameObject go = Instantiate(accessoryPrefab, accessoriesContainer);

                AccessoryButton script = go.GetComponent<AccessoryButton>();
                script.Setup(i, true);

                accessories.Add(go);
            }
        }

        public void SetupEquippedWeaponSetButtons()
        {
        }

        public void SetupEquippedAccessoryButtons()
        {
        }

        public void SetPcData(PcData pcData)
        {
            for (int i = 0; i < (int)EquipmentSlot.Number; i++)
            {
                EquippedItemButton script = equippedItems[i].GetComponent<EquippedItemButton>();

                if (pcData.Inventory.EquippedItems[i] != null)
                {
                    script.SetItemData(pcData.Inventory.EquippedItems[i]);
                }
                else
                    script.SetItemData(null);
            }

            for (int i = 0; i < CharacterInventory.MaximumAccessories; i++)
            {
                AccessoryButton script = accessories[i].GetComponent<AccessoryButton>();

                if (pcData.Inventory.Accessories[i] != null)
                {
                    script.SetItemData(pcData.Inventory.Accessories[i]);
                }
                else
                {
                    script.Clear();
                }

                if (i < pcData.Inventory.AccessoryLimit)
                {
                    script.Enable();
                }
                else
                {
                    script.Disable();
                }
            }
        }
    }
}