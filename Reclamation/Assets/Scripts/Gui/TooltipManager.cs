﻿using Reclamation.Abilities;
using Reclamation.Equipment;
using Reclamation.Misc;
using Reclamation.World;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui
{
    public class TooltipManager : Singleton<TooltipManager>
    {
        [SerializeField] Tooltip primaryTooltip;
        //[SerializeField] Tooltip secondaryTooltip;

        public void DisplayItem(ItemData item)
        {
            if (item == null) return;

            primaryTooltip.SetItem(item);
            Show();
        }

        public void DisplayAbility(Ability ability)
        {
            if (ability == null) return;

            primaryTooltip.SetAbility(ability);
            Show();
        }

        public void DisplayResource(int index)
        {
            primaryTooltip.SetResource(StockpileManager.instance.Resources[index]);
            Show();
        }

        public void Initialize()
        {
            Clear();
            Hide();
        }

        private void Clear()
        {
            primaryTooltip.Clear();
        }

        public void Show()
        {
            primaryTooltip.Show();
        }

        public void Hide()
        {
            primaryTooltip.Hide();
        }
    }
}