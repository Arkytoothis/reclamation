﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Audio;
using Reclamation.Gui;
using Reclamation.Equipment;
using Reclamation.Misc;
using UnityEngine.EventSystems;

public class AccessoryButton : GuiElement
{
    [SerializeField] ItemData itemData;
    [SerializeField] AccessoryButtonData skinData;
    [SerializeField] Image itemImage;
    [SerializeField] TMP_Text stackSizeLabel;
    [SerializeField] UnityEngine.UI.Button button;

    [SerializeField] Image cooldownImage;
    [SerializeField] TMP_Text cooldownLabel;
    //[SerializeField] float cooldown;
    //[SerializeField] bool isCooldown = false;

    [SerializeField] int index = -1;

    public int Index { get { return index; } }

    protected override void OnSkinGui()
    {
        base.OnSkinGui();

        button.transition = Selectable.Transition.ColorTint;
        button.colors = skinData.buttonColors;
    }

    public void OnPointerClick()
    {
        AudioManager.instance.PlaySound(skinData.clickSound, false);
    }

    public void Setup(int index, bool unlocked)
    {
        Clear();
        this.index = index;
    }

    public void SetItemData(ItemData item)
    {
        if (item != null)
        {
            itemData = new ItemData(item);
            itemImage.enabled = true;
            itemImage.sprite = SpriteManager.instance.GetItemIcon(itemData.IconKey);
        }
        else
        {
            Clear();
        }
    }

    public void Clear()
    {
        itemData = null;

        //isCooldown = false;
        //cooldown = 0;
        cooldownLabel.text = "";
        cooldownImage.fillAmount = 0;

        itemImage.enabled = false;
        stackSizeLabel.text = "";
        index = -1;
    }

    public void Enable()
    {
        button.interactable = true;
    }

    public void Disable()
    {
        button.interactable = false;
    }

    public void OnPointerUp(BaseEventData bed)
    {
        //PointerEventData ped = (PointerEventData)bed;

        AudioManager.instance.PlaySound(skinData.clickSound, false);
    }

    public void OnPointerEnter(BaseEventData bed)
    {
        TooltipManager.instance.DisplayItem(itemData);
    }

    public void OnPointerExit(BaseEventData bed)
    {
        TooltipManager.instance.Hide();
    }
}
