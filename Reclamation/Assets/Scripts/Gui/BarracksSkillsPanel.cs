﻿using Reclamation.Characters;
using Reclamation.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class BarracksSkillsPanel : Panel
    {
        [SerializeField] GameObject attributeElementPrefab;
        [SerializeField] List<GameObject> attributeElements;
        [SerializeField] Transform attributeElementsParent;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);
        }

        public void SetPcData(PcData pcData)
        {
            attributeElementsParent.Clear();

            foreach(KeyValuePair<Skill, Attribute> kvp in pcData.Attributes.GetSkills())
            {
                GameObject go = Instantiate(attributeElementPrefab, attributeElementsParent);

                AttributeElement element = go.GetComponent<AttributeElement>();
                Attribute attribute = pcData.Attributes.GetSkill(kvp.Key);
                element.SetData(kvp.Key.ToString(), attribute);
                attributeElements.Add(go);
            }
        }
    }
}