﻿using Reclamation.Characters;
using Reclamation.World;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class ArmoryScreen : GameScreen
    {
        [SerializeField] CharacterListPanel listPanel;
        [SerializeField] ArmoryQuartermasterPanel quartermasterPanel;
        [SerializeField] ArmoryForgemasterPanel forgemasterPanel;
        [SerializeField] ArmoryStockpilePanel stockpilePanel;
        [SerializeField] ArmoryResourcesPanel resourcesPanel;

        [SerializeField] UnityEngine.UI.Button quatermasterButton;
        [SerializeField] UnityEngine.UI.Button forgemasterButton;

        public override void Initialize()
        {
            base.Initialize();

            listPanel.Initialize(this);
            quartermasterPanel.Initialize(this);
            forgemasterPanel.Initialize(this);
            stockpilePanel.Initialize(this);
            resourcesPanel.Initialize(this);

            OpenQuartermaster();
        }

        public override void Open()
        {
            base.Open();
            SelectPc(0);
        }

        public override void Close()
        {
            base.Close();
        }

        public override bool SelectPc(int index)
        {
            PcData pcData = PlayerManager.instance.GetPcData(index);

            quartermasterPanel.SetPcData(pcData);
            forgemasterPanel.SetPcData(pcData);
            stockpilePanel.SetPcData(pcData);
            resourcesPanel.SetPcData(pcData);

            return true;
        }

        public void OpenQuartermaster()
        {
            quartermasterPanel.gameObject.SetActive(true);
            forgemasterPanel.gameObject.SetActive(false);
        }

        public void OpenForgemaster()
        {
            quartermasterPanel.gameObject.SetActive(false);
            forgemasterPanel.gameObject.SetActive(true);
        }
    }
}