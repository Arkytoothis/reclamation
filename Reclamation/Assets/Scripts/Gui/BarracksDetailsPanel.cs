﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Reclamation.Characters;
using UnityEngine.UI;

namespace Reclamation.Gui.World
{
    public class BarracksDetailsPanel : Panel
    {
        [SerializeField] TMP_Text nameLabel;
        [SerializeField] TMP_Text detailsLabel;
        [SerializeField] TMP_Text levelLabel;
        [SerializeField] TMP_Text expLabel;
        [SerializeField] Slider expSlider;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);
        }

        public void SetPcData(PcData pcData)
        {
            if (pcData != null)
            {
                nameLabel.text = pcData.Name.ShortName;
                detailsLabel.text = "Level " + pcData.Level + " " + pcData.RaceKey + " " + pcData.ProfessionKey;

                expSlider.value = (float)pcData.Experience / (float)pcData.ExpToLevel;
                levelLabel.text = "Level " + pcData.Level;
                expLabel.text = "Experience: " + pcData.Experience + "/" + pcData.ExpToLevel;
            }
        }
    }
}