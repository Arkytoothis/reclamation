﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Equipment;
using Reclamation.Misc;
using Reclamation.Abilities;
using Reclamation.World;
using DG.Tweening;

namespace Reclamation.Gui
{
    public class Tooltip : MonoBehaviour
    {
        [SerializeField] TMP_Text nameLabel;
        [SerializeField] TMP_Text descriptionLabel;
        [SerializeField] TMP_Text detailsLabel;
        [SerializeField] TMP_Text levelLabel;

        [SerializeField] Image iconImage;
        [SerializeField] Slider durabilitySlider;
        [SerializeField] TMP_Text durabilityLabel;
        [SerializeField] Slider experienceSlider;
        [SerializeField] TMP_Text experienceLabel;

        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            transform.position = new Vector3(5f, 5f, 0f);
        }

        public void SetItem(ItemData item)
        {
            nameLabel.text = "<color=" + Globals.RarityColors[(int)item.Rarity] + ">" + item.Name;
            iconImage.sprite = SpriteManager.instance.GetItemIcon(item.IconKey);

            descriptionLabel.text = item.description;
            detailsLabel.text = item.attributesTooltip;

            levelLabel.text = "Lvl " + item.Power;
            durabilitySlider.enabled = true;
            durabilitySlider.value = (float)item.DurabilityMax / (float)item.DurabilityCur;
            durabilityLabel.text = item.DurabilityCur + "/" + item.DurabilityMax;

            experienceSlider.enabled = true;
            experienceSlider.value = 0.5f;
            experienceLabel.text = "0/0";
        }

        public void SetAbility(Ability ability)
        {
            nameLabel.text = ability.Name;
            iconImage.sprite = SpriteManager.instance.GetAbilityIcon(ability.SpriteKey);

            descriptionLabel.text = ability.Description;
            detailsLabel.text = ability.ToString();

            levelLabel.text = "";
            durabilitySlider.enabled = false;
            experienceSlider.enabled = false;
        }

        public void SetResource(ResourceData resource)
        {
            nameLabel.text = resource.Key;
            iconImage.sprite = SpriteManager.instance.GetGuiIcon(resource.Key);

            descriptionLabel.text = "";
            detailsLabel.text = "";
            levelLabel.text = "";
            durabilitySlider.enabled = false;
            experienceSlider.enabled = false;
        }

        public void Clear()
        {
            nameLabel.text = "Name";

            levelLabel.text = "";
            iconImage.sprite = null;

            descriptionLabel.text = "Description";
            detailsLabel.text = "Details";

            durabilitySlider.value = 1;
            experienceSlider.value = 1;
        }

        public void Show()
        {
            transform.DOMoveX(5f, 0.25f);
        }

        public void Hide()
        {
            transform.DOMoveX(-650f, 0.25f);
        }
    }
}