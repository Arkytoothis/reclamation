﻿using Reclamation.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Reclamation.Misc;

namespace Reclamation.Gui.World
{
    public class ArmoryQuartermasterPanel : Panel
    {
        [SerializeField] GameObject equippedButtonPrefab;
        [SerializeField] Transform equippedButtonsParent;
        [SerializeField] List<GameObject> equippedButtons;

        [SerializeField] GameObject accessoryButtonPrefab;
        [SerializeField] Transform accessoryButtonsParent;
        [SerializeField] List<GameObject> accessoryButtons;

        [SerializeField] TMP_Text nameLabel;
        [SerializeField] TMP_Text detailsLabel;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);

            equippedButtons = new List<GameObject>();
            for (int i = 0; i < (int)EquipmentSlot.Number; i++)
            {
                GameObject buttonObject = Instantiate(equippedButtonPrefab, equippedButtonsParent);
                buttonObject.name = ((EquipmentSlot)i).ToString() + " button";

                EquippedItemButton buttonScript = buttonObject.GetComponent<EquippedItemButton>();
                buttonScript.Setup(i, ((EquipmentSlotAbb)i).ToString());

                equippedButtons.Add(buttonObject);
            }

            accessoryButtons = new List<GameObject>();
            for (int i = 0; i < CharacterInventory.MaximumAccessories; i++)
            {
                GameObject buttonObject = Instantiate(accessoryButtonPrefab, accessoryButtonsParent);
                buttonObject.name = ((EquipmentSlot)i).ToString() + " button";

                AccessoryButton buttonScript = buttonObject.GetComponent<AccessoryButton>();
                buttonScript.Setup(i, true);

                accessoryButtons.Add(buttonObject);
            }
        }

        public void SetPcData(PcData pcData)
        {
            if (pcData == null) return;

            nameLabel.text = pcData.Name.FullName;
            detailsLabel.text = "Level " + pcData.Level + " " + pcData.RaceKey + " " + pcData.ProfessionKey;

            for (int i = 0; i < (int)EquipmentSlot.Number; i++)
            {
                EquippedItemButton script = equippedButtons[i].GetComponent<EquippedItemButton>();

                if (pcData.Inventory.EquippedItems[i] != null)
                {
                    script.SetItemData(pcData.Inventory.EquippedItems[i]);
                }
                else
                    script.SetItemData(null);
            }

            for (int i = 0; i < CharacterInventory.MaximumAccessories; i++)
            {
                AccessoryButton script = accessoryButtons[i].GetComponent<AccessoryButton>();

                if (pcData.Inventory.Accessories[i] != null)
                {
                    script.SetItemData(pcData.Inventory.Accessories[i]);
                }
                else
                {
                    script.Clear();
                }

                if (i < pcData.Inventory.AccessoryLimit)
                {
                    script.Enable();
                }
                else
                {
                    script.Disable();
                }
            }
        }
    }
}