﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Misc;
using Reclamation.World;
using UnityEngine.EventSystems;
using Reclamation.Audio;

namespace Reclamation.Gui
{
    public class ResourceButton : GuiElement
    {

        [SerializeField] ResourceButtonData skinData;
        [SerializeField] Image itemIcon;
        [SerializeField] TMP_Text keyLabel;
        [SerializeField] TMP_Text valueLabel;

        [SerializeField] int index = -1;

        [SerializeField] UnityEngine.UI.Button button;

        public int Index { get { return index; } }

        protected override void OnSkinGui()
        {
            base.OnSkinGui();

            button = GetComponent<UnityEngine.UI.Button>();
            button.transition = Selectable.Transition.ColorTint;
            button.colors = skinData.buttonColors;
        }

        public void Setup(int index, ResourceData resource)
        {
            this.index = index;
            itemIcon.sprite = SpriteManager.instance.GetGuiIcon(resource.Key);
            keyLabel.text = resource.Key;
            valueLabel.text = "";
        }

        public void UpdateValue()
        {
            ResourceData resource = StockpileManager.instance.Resources[index];
            string s = resource.CurAmount + "/";
            s += resource.MaxAmount + " ";

            if (resource.CurIncome > 0)
                s += "+" + resource.CurIncome;
            else if (resource.CurIncome < 0)
                s += "-" + resource.CurIncome;

            valueLabel.text = s;
        }

        public void OnPointerUp(BaseEventData bed)
        {
            //PointerEventData ped = (PointerEventData)bed;

            AudioManager.instance.PlaySound(skinData.clickSound, false);
        }

        public void OnPointerEnter(BaseEventData bed)
        {
            TooltipManager.instance.DisplayResource(index);
        }

        public void OnPointerExit(BaseEventData bed)
        {
            TooltipManager.instance.Hide();
        }
    }
}