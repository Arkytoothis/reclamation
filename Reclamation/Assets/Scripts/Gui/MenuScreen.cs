﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Misc;

namespace Reclamation.Gui
{
    public class MenuScreen : GameScreen
    {
        public override void Initialize()
        {
            //Debug.Log("MenuScreen Initialized");
            base.Initialize();
        }

        public override void Open()
        {
            base.Open();
        }

        public override void Close()
        {
            base.Close();
        }
    }
}