﻿using Reclamation.Characters;
using Reclamation.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class BarracksAbilitiesPanel : Panel
    {
        [SerializeField] GameObject abilityPrefab;

        [SerializeField] Transform powersParent;
        [SerializeField] List<GameObject> powerButtons;

        [SerializeField] Transform spellsParent;
        [SerializeField] List<GameObject> spellButtons;

        [SerializeField] Transform traitsParent;
        [SerializeField] List<GameObject> traitButtons;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);
            SetupPowerButtons();
            SetupSpellButtons();
            SetupTraitButtons();
        }

        public void SetupPowerButtons()
        {
            powerButtons = new List<GameObject>();

            for (int i = 0; i < 24; i++)
            {
                GameObject go = Instantiate(abilityPrefab, powersParent);

                AbilityButton script = go.GetComponent<AbilityButton>();
                script.Setup(i, false);

                powerButtons.Add(go);
            }
        }

        public void SetupSpellButtons()
        {
            spellButtons = new List<GameObject>();

            for (int i = 0; i < 24; i++)
            {
                GameObject go = Instantiate(abilityPrefab, spellsParent);

                AbilityButton script = go.GetComponent<AbilityButton>();
                script.Setup(i, false);

                spellButtons.Add(go);
            }
        }

        public void SetupTraitButtons()
        {
        }

        public void SetPcData(PcData pcData)
        {
            for (int i = 0; i < powerButtons.Count; i++)
            {
                AbilityButton script = powerButtons[i].GetComponent<AbilityButton>();

                if (i < pcData.Abilities.AvailablePowers.Count)
                {
                    script.SetAbilityData(pcData.Abilities.AvailablePowers[i]);
                    script.Enable();
                }
                else
                {
                    script.Clear();
                    script.Disable();
                }
            }

            for (int i = 0; i < spellButtons.Count; i++)
            {
                AbilityButton script = spellButtons[i].GetComponent<AbilityButton>();

                if (i < pcData.Abilities.AvailableSpells.Count)
                {
                    script.SetAbilityData(pcData.Abilities.AvailableSpells[i]);
                    script.Enable();
                }
                else
                {
                    script.Clear();
                    script.Disable();
                }
            }
        }
    }
}