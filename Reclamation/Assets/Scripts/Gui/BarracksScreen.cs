﻿using Reclamation.Characters;
using Reclamation.Gui.World;
using Reclamation.World;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui
{
    public class BarracksScreen : GameScreen
    {
        [SerializeField] CharacterListPanel listPanel;

        [SerializeField] BarracksDetailsPanel detailsPanel;
        [SerializeField] BarracksPortraitPanel portraitPanel;
        [SerializeField] BarracksBaseAttributesPanel baseAttributesPanel;
        [SerializeField] BarracksDerivedAttributesPanel derivedAttributesPanel;
        [SerializeField] BarracksSkillsPanel skillsPanel;
        [SerializeField] BarracksEquipmentPanel equipmentPanel;
        [SerializeField] BarracksAbilitiesPanel abilitiesPanel;

        public override void Initialize()
        {
            base.Initialize();

            listPanel.Initialize(this);
            detailsPanel.Initialize(this);
            portraitPanel.Initialize(this);
            baseAttributesPanel.Initialize(this);
            derivedAttributesPanel.Initialize(this);
            skillsPanel.Initialize(this);
            equipmentPanel.Initialize(this);
            abilitiesPanel.Initialize(this);
        }

        public override void Open()
        {
            base.Open();
            SelectPc(0);
        }

        public override void Close()
        {
            base.Close();
        }

        public override bool SelectPc(int index)
        {
            PcData pcData = PlayerManager.instance.GetPcData(index);

            portraitPanel.LoadPortrait(index);
            detailsPanel.SetPcData(pcData);
            baseAttributesPanel.SetPcData(pcData);
            derivedAttributesPanel.SetPcData(pcData);
            skillsPanel.SetPcData(pcData);
            equipmentPanel.SetPcData(pcData);
            abilitiesPanel.SetPcData(pcData);

            return true;
        }
    }
}