﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Gui.World
{
    public class ResourcePanel : MonoBehaviour
    {
        [SerializeField] GameObject buttonPrefab;
        [SerializeField] Transform buttonsParent;
        [SerializeField] List<ResourceButton> buttons;

        private IEnumerator coroutine;

        public void Initialize()
        {
            SetupButtons();
        }

        public void SetupButtons()
        {
            for (int i = 0; i < StockpileManager.instance.Resources.Count; i++)
            {
                GameObject buttonObject = Instantiate(buttonPrefab, buttonsParent);
                buttonObject.name = StockpileManager.instance.Resources[i].Key + " button";

                ResourceButton buttonScript = buttonObject.GetComponent<ResourceButton>();
                buttonScript.Setup(i, StockpileManager.instance.Resources[i]);

                buttons.Add(buttonScript);
            }

            coroutine = WaitAndPrint(1.0f);

            StartCoroutine(coroutine);
        }

        public void UpdateResources()
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].UpdateValue();
            }
        }

        private IEnumerator WaitAndPrint(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            UpdateResources();
        }
    }
}