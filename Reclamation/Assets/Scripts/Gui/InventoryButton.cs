﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Audio;
using Reclamation.Gui;
using Reclamation.Equipment;
using Reclamation.Misc;
using UnityEngine.EventSystems;

public class InventoryButton : GuiElement
{
    [SerializeField] InventoryButtonData skinData;
    [SerializeField] Image itemImage;
    [SerializeField] TMP_Text stackSizeLabel;
    [SerializeField] UnityEngine.UI.Button button;

    [SerializeField] int index = -1;

    [SerializeField] ItemData itemData;

    public int Index { get { return index; } }

    protected override void OnSkinGui()
    {
        base.OnSkinGui();

        button.transition = Selectable.Transition.ColorTint;
        button.colors = skinData.buttonColors;
    }

    public void Setup(int index, string slot)
    {
        Clear();
        this.index = index;
    }

    public void SetItemData(ItemData item)
    {
        if (item != null)
        {
            itemData = new ItemData(item);
            itemImage.enabled = true;
            itemImage.sprite = SpriteManager.instance.GetItemIcon(itemData.IconKey);
        }
        else
        {
            Clear();
        }
    }

    public void Clear()
    {
        itemData = null;

        itemImage.enabled = false;
        itemImage.sprite = null;
        stackSizeLabel.text = "";
        index = -1;
    }

    public void OnPointerUp(BaseEventData bed)
    {
        //PointerEventData ped = (PointerEventData)bed;

        AudioManager.instance.PlaySound(skinData.clickSound, false);
    }

    public void OnPointerEnter(BaseEventData bed)
    {
        TooltipManager.instance.DisplayItem(itemData);
    }

    public void OnPointerExit(BaseEventData bed)
    {
        TooltipManager.instance.Hide();
    }
}
