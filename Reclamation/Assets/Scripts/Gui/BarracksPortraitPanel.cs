﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.World;
using TMPro;
using UnityEngine.UI;
using Reclamation.Misc;

namespace Reclamation.Gui.World
{
    public class BarracksPortraitPanel : Panel
    {
        [SerializeField] TMP_Text levelLabel;
        [SerializeField] RawImage portraitImage;

        public override void Initialize(GameScreen screen)
        {
            base.Initialize(screen);
        }

        public void LoadPortrait(int index)
        {
            portraitImage.texture = PortraitRoom.instance.PcMounts[index].FarCamera.targetTexture;
        }
    }
}