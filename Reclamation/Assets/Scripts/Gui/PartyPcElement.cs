﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Reclamation.Characters;
using Reclamation.Misc;

namespace Reclamation.Gui
{
    public class PartyPcElement : MonoBehaviour
    {
        [SerializeField] TMP_Text nameLabel;
        [SerializeField] TMP_Text levelLabel;
        [SerializeField] RawImage rawImage;
        [SerializeField] UnityEngine.UI.Button button;

        public void SetData(PcData pc)
        {
            if (pc != null)
            {
                rawImage.texture = PortraitRoom.instance.PcMounts[pc.WorldIndex].RtClose;

                nameLabel.text = pc.Name.FirstName + "\n" + pc.Name.LastName;
                levelLabel.text = "Level " + pc.Level;
                button.interactable = true;
            }
            else
            {
                rawImage.texture = null;
                nameLabel.text = "";
                levelLabel.text = "";
                button.interactable = false;
            }
        }
    }
}