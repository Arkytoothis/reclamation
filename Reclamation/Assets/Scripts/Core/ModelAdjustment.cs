﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reclamation.Misc
{
    public class ModelAdjustment : MonoBehaviour
    {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
    }
}