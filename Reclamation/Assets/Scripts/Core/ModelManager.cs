﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reclamation.Characters;
using Reclamation.Encounter;
using Reclamation.Equipment;

namespace Reclamation.Misc
{
    public class ModelManager : Singleton<ModelManager>
    {
        private Dictionary<string, GameObject> characterPrefabs;
        public Dictionary<string, GameObject> CharacterPrefabs { get { return characterPrefabs; } }

        private Dictionary<string, GameObject> itemPrefabs;
        public Dictionary<string, GameObject> ItemPrefabs { get { return itemPrefabs; } }

        private Dictionary<string, GameObject> hairPrefabs;
        public Dictionary<string, GameObject> HairPrefabs { get { return hairPrefabs; } }

        private Dictionary<string, GameObject> beardPrefabs;
        public Dictionary<string, GameObject> BeardPrefabs { get { return beardPrefabs; } }

        public List<GameObject> characters = new List<GameObject>();

        public GameObject emptyPcPrefab;

        public RuntimeAnimatorController maleAnimator;
        public RuntimeAnimatorController malePortraitAnimator;
        public RuntimeAnimatorController femaleAnimator;
        public RuntimeAnimatorController femalePortraitAnimator;

        public string defaultItemPath = "Equipment/Prefabs/USING/";
        public string defaultCharacterPath = "Characters/Prefabs/";

        public GameObject GetItemPrefab(string key)
        {
            if (itemPrefabs.ContainsKey(key) == false)
            {
                Debug.Log("Prefab for " + key + " does not exist.");
                return null;
            }
            else
            {
                return itemPrefabs[key];
            }
        }

        public void Initialize()
        {
            itemPrefabs = new Dictionary<string, GameObject>();
            characterPrefabs = new Dictionary<string, GameObject>();
            hairPrefabs = new Dictionary<string, GameObject>();
            beardPrefabs = new Dictionary<string, GameObject>();

            LoadPrefabs();
        }

        public void LoadPrefabs()
        {
            LoadHairPrefabs();
            LoadBeardPrefabs();
            LoadCharacterPrefabs();
            LoadEquipmentPrefabs();
        }

        void LoadHairPrefabs()
        {
            Object[] prefabs;

            prefabs = Resources.LoadAll<GameObject>("Characters/Prefabs/Hair");

            foreach (GameObject go in prefabs)
            {
                hairPrefabs.Add(go.name, go);
            }
        }

        void LoadBeardPrefabs()
        {
            Object[] prefabs;

            prefabs = Resources.LoadAll<GameObject>("Characters/Prefabs/Beards");

            foreach (GameObject go in prefabs)
            {
                beardPrefabs.Add(go.name, go);
            }
        }

        void LoadEquipmentPrefabs()
        {
            foreach (KeyValuePair<string, ItemDefinition> kvp in Database.Items)
            {
                GameObject go = GetItemPrefab(kvp.Value);
                itemPrefabs.Add(kvp.Value.MeshKey, go);
            }
        }

        void LoadCharacterPrefabs()
        {
            emptyPcPrefab = Resources.Load<GameObject>(defaultCharacterPath + "Character Controllers/Empty PC");

            foreach (KeyValuePair<string, Race> kvp in Database.Races)
            {
                if (kvp.Value.maleModelPath != "" && kvp.Value.femaleModelPath != "")
                {
                    GameObject go = Resources.Load<GameObject>(defaultCharacterPath + "Races/" + kvp.Value.maleModelPath);
                    characterPrefabs.Add(kvp.Value.Name + " Male", go);
                    characters.Add(go);

                    go = Resources.Load<GameObject>(defaultCharacterPath + "Races/" + kvp.Value.femaleModelPath);
                    characterPrefabs.Add(kvp.Value.Name + " Female", go);
                    characters.Add(go);
                }
            }
        }

        void SetupAnimator(GameObject model, Gender gender, bool portrait)
        {
            Animator animator = model.GetComponent<Animator>();

            if (portrait == true)
            {
                if (gender == Gender.Male)
                    animator.runtimeAnimatorController = malePortraitAnimator;
                else if (gender == Gender.Female)
                    animator.runtimeAnimatorController = femalePortraitAnimator;
            }
            else if (portrait == false)
            {
                if (gender == Gender.Male)
                    animator.runtimeAnimatorController = maleAnimator;
                else if (gender == Gender.Female)
                    animator.runtimeAnimatorController = femaleAnimator;
            }
        }

        public GameObject SpawnCharacter(Transform parent, Vector3 position, Gender gender, string race, string hair, string beard)
        {
            GameObject empty = null;
            empty = Instantiate(emptyPcPrefab);
            empty.transform.position = Vector3.zero;
            empty.transform.SetParent(parent);
            empty.transform.position = position;

            SpawnModel(empty.transform, gender, race, hair, beard, false);

            return empty;
        }

        public GameObject SpawnModel(Transform parent, Gender gender, string race, string hair, string beard, bool portrait)
        {
            GameObject characterModel = Instantiate(characterPrefabs[race + " " + gender.ToString()], parent, false);
            characterModel.transform.SetParent(parent);

            SetupAnimator(characterModel, gender, portrait);
            SetupFeatures(characterModel, hair, beard);

            return characterModel;
        }

        public void EquipCharacter(GameObject pcObject, PcData pcData)
        {
            CharacterRenderer renderer = pcObject.GetComponentInChildren<CharacterRenderer>();
            SpawnItem(renderer, pcData, (int)EquipmentSlot.Right_Hand);
            SpawnItem(renderer, pcData, (int)EquipmentSlot.Left_Hand);
            SpawnItem(renderer, pcData, (int)EquipmentSlot.Head);
        }

        public void SetupFeatures(GameObject model, string hair, string beard)
        {
            CharacterRenderer renderer = model.GetComponent<CharacterRenderer>();

            if (hair != "")
            {
                renderer.SetHair(GetHairModel(hair));
            }

            if (beard != "")
            {
                renderer.SetBeard(GetBeardModel(beard));
            }
        }

        public void SpawnItem(CharacterRenderer renderer, PcData pcData, int slot)
        {
            if (pcData.Inventory.EquippedItems[slot] != null)
            {
                if (renderer.Mounts[slot].childCount > 0)
                {
                    Destroy(renderer.Mounts[slot].GetChild(0).gameObject);
                }

                GameObject itemModel = Instantiate(itemPrefabs[pcData.Inventory.EquippedItems[slot].MeshKey]);
                itemModel.transform.SetParent(renderer.Mounts[slot], false);
                itemModel.name = pcData.Inventory.EquippedItems[slot].Name;
            }
        }

        public GameObject GetItemPrefab(ItemDefinition item)
        {
            if (item == null || item.MeshKey == defaultItemPath) return null;

            GameObject go = Resources.Load<GameObject>(defaultItemPath + item.MeshKey);

            if (go == null)
            {
                Debug.Log("Loading Item failed. " + defaultItemPath + item.MeshKey);
            }
            else
            {
                go.transform.localScale = Vector3.one;
                //Debug.Log("Loading Item " + go.name + " success. " + key);
            }

            return go;
        }

        public GameObject GetCharacterPrefab(Vector3 scale, string path)
        {
            if (path == defaultCharacterPath) return null;

            GameObject go = characterPrefabs[path];

            if (go == null)
            {
                Debug.Log("Loading Body failed. " + path);
            }
            else
            {
                go.transform.localScale = scale;
                //Debug.Log("Loading Body " + go.name + " success. " + path);
            }

            return go;
        }

        public GameObject GetHairModel(string hairKey)
        {
            if (hairPrefabs.ContainsKey(hairKey))
            {
                return hairPrefabs[hairKey];
            }
            else
            {
                Debug.Log("Loading Hair failed. " + hairKey);
                return null;
            }
        }

        public GameObject GetBeardModel(string beardKey)
        {
            if (beardPrefabs.ContainsKey(beardKey))
            {
                return beardPrefabs[beardKey];
            }
            else
            {
                Debug.Log("Loading Beard failed. " + beardKey);
                return null;
            }
        }
    }
}