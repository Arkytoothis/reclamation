﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using System.Linq;

namespace Reclamation.Misc
{
    public class SpriteManager : Singleton<SpriteManager>
    {
        bool initialized = false;

        private Dictionary<string, Sprite> guiIcons = new Dictionary<string, Sprite>();
        private Dictionary<string, Sprite> itemIcons = new Dictionary<string, Sprite>();
        private Dictionary<string, Sprite> abilityIcons = new Dictionary<string, Sprite>();

        public Dictionary<string, Sprite> GuiIcons { get { return guiIcons; } }
        public Dictionary<string, Sprite> ItemIcons { get { return itemIcons; } }
        public Dictionary<string, Sprite> AbilityIcons { get { return abilityIcons; } }

        private void Awake()
        {
            Reload();
        }

        public void Initialize()
        {
            if (initialized == false)
            {
                initialized = true;

                Sprite[] guiSprites = Resources.LoadAll<Sprite>("Sprites\\Icons");

                foreach (Sprite s in guiSprites)
                {
                    guiIcons[s.name] = s;
                }

                Sprite[] abilitySprites = Resources.LoadAll<Sprite>("Sprites\\Abilities");

                foreach (Sprite s in abilitySprites)
                {
                    abilityIcons[s.name] = s;
                }

                Sprite[] itemSprites = Resources.LoadAll<Sprite>("Sprites\\Items");

                foreach (Sprite s in itemSprites)
                {
                    itemIcons[s.name] = s;
                }
            }
        }

        void LoadSpritesFromFolder(string path, Dictionary<string, Sprite> dict, bool log)
        {
            Texture2D[] textures = (Texture2D[])Resources.LoadAll<Texture2D>(path);

            for (int i = 0; i < textures.Length; i++)
            {
                Sprite sprite = Sprite.Create(textures[i], new Rect(0f, 0f, textures[i].width, textures[i].height), new Vector2(0f, 0f), 32f);
                sprite.texture.filterMode = FilterMode.Point;

                dict.Add(textures[i].name, sprite);

                if (log == true)
                {
                    Debug.Log(textures[i].name);
                }
            }
        }

        public Sprite GetItemIcon(string key)
        {
            if (itemIcons.ContainsKey(key) == false)
            {
                Debug.LogWarning("itemSprites key: " + key + " does not exist");
                return itemIcons["blank"];
            }
            else
            {
                return itemIcons[key];
            }
        }

        public Sprite GetAbilityIcon(string key)
        {
            if (abilityIcons.ContainsKey(key) == false)
            {
                Debug.LogWarning("abilitySprites key: " + key + " does not exist");
                return abilityIcons["blank"];
            }
            else
            {
                return abilityIcons[key];
            }
        }

        public Sprite GetGuiIcon(string key)
        {
            if (guiIcons.ContainsKey(key) == false)
            {
                Debug.LogWarning("guiIcons key: " + key + " does not exist");
                return guiIcons["blank"];
            }
            else
            {
                return guiIcons[key];
            }
        }
    }
}